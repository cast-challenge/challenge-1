
# Challenge 1 : A common interface for Securities

Our contract interface allow the manipulation of token in a unified process.
During the process, the DvP contract will act on the underlying register. 
The DvP contract can be used to display a bulletin board of trades.

Our interface can interact with any of the following protocols :

```shell
- ForgeBond
- ERC20
- ERC1400
```

Our interface will allow you to interact with a common set of features :

```shell
- Set Allowance (authorizeOperator / setTokenControllers)
- Subscription (Mint)
- Transfer (Transfer)
- Redeem (Burn)
````



# Challenge 2 : atomic settlement of the trade

Once the tokens are set, the DvP will allow the following operation :

```shell
- Optionally Set trade executers (setTradeExecuters)
- Optionally Set price oracle (setTokenPrice)

- Allow DvP to interact on behalf of the users (setTokenControllers)
- Anyone can post a new Offer (requestTrade)
- Seller and buyer accept the offer (acceptTrade)
- Registrar must accept the trade (approveTrade)
- Settlar or executer must confirm the trade (executeTrade)
- Once executed, the tokens are swapped in an atomic transaction
```

Currently our interface can be used to swap one of following protocol :
```shell
- ForgeBond
- ERC20
- ERC1400
- ERC721
- Ethereum
```
Ex :
Swap 10 ForgeBond against 10 StableCoin
Swap 1 NFT against 1 ForgeBond
Swap ERC1400 against ForgeBond
Swap ERC20 against ethereum

Our goal was to show a real-life implementation of our interpretation of a decentralized and compliant bulletin board exchange.

# Project Architecture
contracts : we combined tools and library from ForgeBond and ERC1400
deploy : run hh deploy to deploy your smart contracts
frontend : a Next.js + Web3 implementation of the DvP Contract
test : run hh test to visualize the process of DvP implementation, using ForgeBond, ERC20 and ERC1400 securities standard.
scripts : an helper to fund wallets or initiate allowances

About DVP.sol : A framework for DvP process based on Gauthier Petetin's work (ConsenSys), and recentrly decommissioned (open source). I had the chance to meet him personnaly and learn a bit more about the vision and limitations of this project. We added support for ForgeBond support in order to unify Securities trade.
Each instance of the DVP.sol deployed can be refered to as a bulletin board, and can be easily implemented as a platform's trade registry.
Our next steps would be :
    - Add a "hold" feature, a mix of allowance and escrow, allowing an atomic swap with the certainty of existing balance on accounts.
    - A multichain hold, that would allow to move value from one chain to another.




