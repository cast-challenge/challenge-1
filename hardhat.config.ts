import * as dotenv from "dotenv";

import { HardhatUserConfig, task } from "hardhat/config";
import "@nomiclabs/hardhat-etherscan";
import "@nomiclabs/hardhat-waffle";
import "@typechain/hardhat";
import "hardhat-gas-reporter";
import "solidity-coverage";
import "@nomiclabs/hardhat-web3";
import "hardhat-deploy";

dotenv.config();

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address + " " + account.getBalance());
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

const config: HardhatUserConfig = {
  paths: {
    artifacts: "./frontend/ABI",
    deployments: "./frontend/deployments",
    // sources: "./frontend/src",
    // tests: "./frontend/test",
  },
  typechain: {
    outDir: "./../frontend/types",
    target: "ethers-v5",
  },
  solidity: {
    compilers: [
      {
        version: "0.8.4",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
    ],
  },
  networks: {
    ropsten: {
      url: process.env.ROPSTEN_URL || "",
      accounts:
        process.env.PRIVATE_KEY !== undefined ? [process.env.PRIVATE_KEY] : [],
    },
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
  },
  etherscan: {
    apiKey: process.env.ETHERSCAN_API_KEY,
  },
  namedAccounts: {
    owner: 0,
    operator: 1,
    controller: 2,
    controller_alternative1: 3,
    controller_alternative2: 4,
    tokenHolder: 5,
    recipient: 6,
    unknown: 7,
    ISSUER_ACCOUNT: "0xd13D0535322F1752B50b1515943F4d48213624B7",
    OWNER_ACCOUNT: 10, // "0xFEF4d3391061DC119e834Ef93DB710201F829633"
    CONTROLLER_ACCOUNT: "0xf4fA6bC4aDbB5281275865E9AA6f0b5F492B6DC8",
    HOLDER_ACCOUNT_1: "0xE2a5eF6bC60e24614b748D0AAf654bd4Df5e5fc5",
    HOLDER_ACCOUNT_2: "0x812a21C3CB31f58BeFc831018BAF9102c6DB755c",
    HOLDER_ACCOUNT_3: "0x933Ede12b36dbD930B24efe50B76480A37286868",
    HOLDER_ACCOUNT_4: "0x5358Ff65c08E7188D555E28e3f4d711aAA8763Cb",
    ERC1820_DEPLOYER_ADDRESS: "0xa990077c3205cbDf861e17Fa532eeB069cE9fF96",
  },
};

export default config;
