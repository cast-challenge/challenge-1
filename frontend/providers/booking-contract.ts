import { useContract } from "wagmi";
import { UserBooking } from "../interfaces";


const getUserBookings = (userBookings: UserBooking[]) => {};

const useGetContract = (contractAddress: string, ensRegistryABI: string) => {
  console.log(ensRegistryABI);

  const contract = useContract({
    addressOrName: contractAddress,
    contractInterface: ensRegistryABI,
  });

  const currentOwner = async () => {
    return await contract.owner();
  };
  return { currentOwner };
};

export { useGetContract };
