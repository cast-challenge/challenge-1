import { BigNumberish, BytesLike } from "ethers";
export interface TokenData {
  address: string;
  amount: string;
  partition: string;
  standard: string;
  accepted: boolean;
  approved: boolean;
}
export const FORGE_BOND_CONTRACT_NAME = "ForgeBond";

interface ERC20ContractInput {
  name: string;
  symbol: string;
  denomination: BigNumberish;
}

export interface ForgeBondContractInput {
  initialSupply: BigNumberish;
  isinCode: string;
  name: string;
  symbol: string;
  denomination: BigNumberish;
  divisor: BigNumberish;
  startDate: BigNumberish;
  initialMaturityDate: BigNumberish;
  firstCouponDate: BigNumberish;
  couponFrequencyInMonths: BigNumberish;
  interestRateInBips: BigNumberish;
  callable: boolean;
  isSoftBullet: boolean;
  softBulletPeriodInMonths: BigNumberish;
  currency: string;
  registrar: string;
  settler: string;
  owner: string;
}

export const FORGE_BOND_PARAMS: ForgeBondContractInput = {
  initialSupply: "100000000",
  isinCode: "US0378331005",
  name: "BondSwap",
  symbol: "REF",
  denomination: "1000",
  divisor: "100",
  startDate: "0",
  initialMaturityDate: "0",
  firstCouponDate: "0",
  couponFrequencyInMonths: "0",
  interestRateInBips: "0",
  callable: true,
  isSoftBullet: false,
  softBulletPeriodInMonths: "0",
  currency: "0x0000000000000000000000000000000000000000",
  registrar: "0x0000000000000000000000000000000000000000",
  settler: "0x0000000000000000000000000000000000000000",
  owner: "0x0000000000000000000000000000000000000000",
};

export interface TradeRequest {
  holder1: string;
  holder2: string;
  executer: string;
  expirationDate: BigNumberish;
  tokenAddress1: string;
  tokenValue1: BigNumberish;
  tokenId1: BytesLike;
  tokenStandard1: BigNumberish;
  tokenAddress2: string;
  tokenValue2: BigNumberish;
  tokenId2: BytesLike;
  tokenStandard2: BigNumberish;
  tradeType: BigNumberish;
}
// address holder1;
//     address holder2;
//     address executer;
//     uint256 expirationDate;
//     bytes tokenData1;
//     bytes tokenData2;
//     TradeType tradeType;
//     State state;
export interface Trade {
  holder1: string;
  holder2: string;
  executer: string;
  expirationDate: BigNumberish;
  tokenData1Raw: BytesLike;
  tokenData2Raw: BytesLike;
  tokenData1?: TokenData;
  tokenData2?: TokenData;
  tradeType: number;
  state: number;
}

export const ERC20_INTERFACE_NAME = "ERC20Token";
export const ERC1400_INTERFACE_NAME = "ERC1400Token";

export const ZERO_BYTES32 =
  "0x0000000000000000000000000000000000000000000000000000000000000000";
export const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";
export const ZERO_BYTE = "0x";

export const partitionFlag =
  "0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"; // Flag to indicate a partition change
export const otherFlag =
  "0xdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"; // Other flag

export const HEX_RESERVED =
  "7265736572766564000000000000000000000000000000000000000000000000"; // reserved in hex
export const HEX_ISSUED =
  "6973737565640000000000000000000000000000000000000000000000000000"; // issued in hex
export const HEX_LOCKED =
  "6c6f636b65640000000000000000000000000000000000000000000000000000"; // locked in hex

export const changeToPartition2 = partitionFlag.concat(HEX_ISSUED);
export const doNotChangePartition = otherFlag.concat(HEX_ISSUED);

export const PARTITION_RESERVED = "0x".concat(HEX_RESERVED);
export const PARTITION_ISSUED = "0x".concat(HEX_ISSUED);
export const PARTITION_LOCKED = "0x".concat(HEX_LOCKED);
