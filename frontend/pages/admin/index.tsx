import { ExclamationIcon } from "@heroicons/react/solid";
import { useEffect, useMemo, useState } from "react";
import { useAccount } from "wagmi";
import WeekView from "../../components/Calendar/WeekView";
import ErrorMessage from "../../components/ErrorMessage";
import Layout from "../../components/Layout";
import TradesTable from "../../components/TradesTable";
import Transactions from "../../components/Transactions";
import { useBookingContract } from "../../hooks/useBookingContract";
import { useDVPContract } from "../../hooks/useDVPContract";

const AdminPage = () => {
  const [isAdmin, setIsAdmin] = useState(false);
  const { contract: dataContract } = useDVPContract();
  const [{ data: accountData }, disconnect] = useAccount({
    fetchEns: true,
  });

  useMemo(async () => {
    if (!dataContract?.signer || !accountData?.connector) {
      if (isAdmin) setIsAdmin(false);
    } else {
      console.log(await dataContract?.owner());
      const isAdmin = (await dataContract?.owner()) === accountData?.address;
      setIsAdmin(isAdmin);
    }
  }, [accountData?.address, accountData?.connector, dataContract, isAdmin]);

  return (
    <Layout>
      {isAdmin || !isAdmin ? (
        <>
          {/* <WeekView /> */}
          <TradesTable />
          <Transactions />
        </>
      ) : (
        <ErrorMessage
          title={"Admin Only"}
          message={"The Admin Page is accessible by every signed in admin."}
        />
      )}
    </Layout>
  );
};
export default AdminPage;
