import { NextPage } from "next";
import DayView from "../../components/Calendar/DayView";
import Layout from "../../components/Layout";
import MyBookings from "../../components/MyBookings";
import RoomItem from "../../components/RoomItem";
import RoomsList from "../../components/OffersLists";

const RoomsPage: NextPage = () => {
  return (
    <Layout>
      <MyBookings></MyBookings>
    </Layout>
  );
};

export default RoomsPage;
