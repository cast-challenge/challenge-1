import { NextPage } from "next";
import BookingSidebar from "../../components/BookingSidebar";
import DayView from "../../components/Calendar/DayView";
import Layout from "../../components/Layout";
import OffersLists from "../../components/OffersLists";
import RoomsList from "../../components/OffersLists";
import { Offer, TradeRequest } from "../../interfaces";
import { useGetEventsQuery, useGetRoomsQuery } from "../../stores/api";

const RoomsPage: NextPage = () => {
  // const { data: offers, error, isLoading, refetch } = useGetRoomsQuery(null);
  const tokenHolder = {
    address: process.env.NEXT_PUBLIC_DVP_CONTRACT_ADDRESS as string,
  };
  const settler = {
    address: process.env.NEXT_PUBLIC_DVP_CONTRACT_ADDRESS as string,
  };
  const recipient = {
    address: process.env.NEXT_PUBLIC_DVP_CONTRACT_ADDRESS as string,
  };
  const contractBondInstance = {
    address: process.env.NEXT_PUBLIC_ERC1400_CONTRACT_ADDRESS as string,
  };
  const contractStableInstance = {
    address: process.env.NEXT_PUBLIC_ERC20_CONTRACT_ADDRESS as string,
  };
  const contractDVPInstance = {
    address: process.env.NEXT_PUBLIC_DVP_CONTRACT_ADDRESS as string,
  };

  const trade: TradeRequest = {
    holder1: tokenHolder.address,
    holder2: recipient.address,
    executer: settler.address,
    expirationDate: 1658798074,
    tokenAddress1: contractBondInstance.address,
    tokenValue1: 100,
    tokenId1: "0x01",
    tokenStandard1: 5, // "0x45524331343030",
    tokenAddress2: contractStableInstance.address,
    tokenValue2: 350,
    tokenId2: "0x02",
    tokenStandard2: 2, // "0x4552433230",
    tradeType: 1,
  };
  const offers: Offer[] = [];
  const offer1: Offer = {
    id: "1",
    name: "Salle 1",
    contractDVPAddress: contractDVPInstance.address,
    price: 100,
    organisationId: "1",
    organisation: {
      id: "1",
      name: "Organisation 1",
    },
    index: 0,
    quantity: 100,
    tokenAddress1: contractDVPInstance.address,
    tokenValue1: "",
    tokenId1: "",
    tokenStandard1: "",
    tokenAddress2: "",
    tokenValue2: "",
    tokenId2: "",
    tokenStandard2: "",
    tradeType: "",
  };
  offers.push(offer1);
  return (
    <Layout>
      {offers && offers.length > 0 && (
        <>
          <div className="flex flex-wrap -mx-4 -mb-4 md:mb-0">
            <div className="w-full md:w-2/3 px-4 mb-4 md:mb-0">
              <OffersLists offers={offers}></OffersLists>
            </div>
            {/* <div className="w-full md:w-1/3 px-4 mb-4 md:mb-0">
              <BookingSidebar></BookingSidebar>
            </div> */}
          </div>
        </>
      )}
    </Layout>
  );
};

export default RoomsPage;
