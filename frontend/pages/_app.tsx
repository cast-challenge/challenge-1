import "../styles/globals.css";
import type { AppProps } from "next/app";
import { connectors } from "../providers/wagmi";
import { Chain, Provider as WagmiProvider } from "wagmi";
import { providers } from "ethers";
import { Provider as ReduxProvider } from "react-redux";
import { store } from "../hooks/store";

// Provider that will be used when no wallet is connected (aka no signer)
const provider = providers.getDefaultProvider("ropsten"); // "http://localhost:8545"
const avalancheChain: Chain = {
  id: 43_114,
  name: "Localhost",
  network: "localhost",
  nativeCurrency: {
    decimals: 18,
    name: "Leth",
    symbol: "LETH",
  },
  rpcUrls: {
    default: "http://localhost:8545",
  },
  testnet: true,
};

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <WagmiProvider
      autoConnect
      connectors={connectors({ chainId: 31337 })}
      provider={provider}
    >
      <ReduxProvider store={store}>
        <Component {...pageProps} />
      </ReduxProvider>
    </WagmiProvider>
  );
}

export default MyApp;
