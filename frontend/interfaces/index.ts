import { BigNumberish, BytesLike } from "ethers";
import { Connector } from "wagmi";

export interface Organisation {
  id: string;
  name: string;
}

export interface Days {
  date: string;
  isCurrentMonth?: boolean;
  isToday?: boolean;
  isSelected?: boolean;
}

export interface Offer {
  organisation?: Organisation;
  id: string;
  name: string;
  price: number;
  quantity: number;
  organisationId: string;
  contractDVPAddress: string;
  index: number;
  tokenAddress1: string;
  tokenValue1: BigNumberish;
  tokenId1: BytesLike;
  tokenStandard1: BigNumberish;
  tokenAddress2: string;
  tokenValue2: BigNumberish;
  tokenId2: BytesLike;
  tokenStandard2: BigNumberish;
  tradeType: BigNumberish;
}

export interface TradeRequest {
  holder1: string;
  holder2: string;
  executer: string;
  expirationDate: BigNumberish;
  tokenAddress1: string;
  tokenValue1: BigNumberish;
  tokenId1: BytesLike;
  tokenStandard1: BigNumberish;
  tokenAddress2: string;
  tokenValue2: BigNumberish;
  tokenId2: BytesLike;
  tokenStandard2: BigNumberish;
  tradeType: BigNumberish;
}
export interface Token {
  id: string;
  name: string;
  price: number;
  contractAddress: string;
  contractStandard: string;
  index: number;
}

export interface Room {
  organisation?: Organisation;
  id: string;
  name: string;
  price: number;
  organisationId: string;
  index: number;
}

export interface UserBooking {
  room: Room;
  start: string;
}

export interface IEvent {
  rooms: Room[];
  name: string;
  id: string;
  start: string;
}

export interface AddToCartState {
  offerId: string;
  timeStart: number;
  timeEnd: number;
  nbrHours: number;
  isEmpty: boolean;
}

export interface Booking {
  imageUrl?: string;
  location?: string;
  id: number;
  date: string;
  time: string;
  datetime: string;
  roomName: string;
}
export interface Account {
  address: string;
  connector: Connector<any, any> | undefined;
  ens:
    | {
        avatar: string | null | undefined;
        name: string;
      }
    | undefined;
}

export interface BIAuthTokenResponse {
  auth_token: string;
  type: string;
  id_user: number;
  expires_in?: number;
}
export interface BIAuthTokenRequest {
  client_id: string;
  client_secret: string;
}
