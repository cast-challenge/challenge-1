import { BigNumber } from "ethers";

export const truncateWallet = (
  address: string,
  startLength: number = 4,
  endLength: number = 4
) => {
  if (!address) {
    return "";
  }
  return `${address.substring(0, startLength)}...${address.substring(
    address.length - endLength
  )}`;
};

export enum StandardType {
  OffChain = "OffChain",
  ETH = "ETH",
  ERC1400 = "ERC1400",
  ERC721 = "ERC721",
  ERC20 = "ERC20",
  ForgeBond = "ForgeBond",
}
export enum StatusType {
  Undefined = "Undefined",
  Pending = "Pending",
  Executed = "Executed",
  Forced = "Forced",
  Cancelled = "Cancelled",
}

export const convertIdToStandard = (id: number) => {
  let standard = "unknown";

  switch (id) {
    case 0:
      standard = StandardType.OffChain;
      break;
    case 1:
      standard = StandardType.ETH;
      break;
    case 2:
      standard = StandardType.ERC20;
      break;
    case 3:
      standard = StandardType.ERC721;
      break;
    case 4:
      standard = StandardType.ERC1400;
      break;
    case 5:
      standard = StandardType.ForgeBond;
      break;
    case 6:
      standard = StandardType.ERC20;
      break;

    default:
      break;
  }

  return standard;
};
export const convertIdToStatus = (id: number) => {
  let standard = "unknown";

  switch (id) {
    case 0:
      standard = StatusType.Undefined;
      break;
    case 1:
      standard = StatusType.Pending;
      break;
    case 2:
      standard = StatusType.Executed;
      break;
    case 3:
      standard = StatusType.Forced;
      break;
    case 4:
      standard = StatusType.Cancelled;
      break;

    default:
      standard = StatusType.Undefined;
      break;
  }

  return standard;
};

export const hexToNumber = (hex: BigNumber) => {
  return parseInt(hex._hex, 16);
};

export const moneyFormatter = (value: number, digits: number = 1) => {
  const lookup = [
    { value: 1, symbol: "" },
    { value: 1e3, symbol: "k" },
    { value: 1e6, symbol: "M" },
    { value: 1e9, symbol: "G" },
    { value: 1e12, symbol: "T" },
    { value: 1e15, symbol: "P" },
    { value: 1e18, symbol: "E" },
  ];
  const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  const item = lookup
    .slice()
    .reverse()
    .find(function (item) {
      return value >= item.value;
    });
  return item
    ? (value / item.value).toFixed(digits).replace(rx, "$1") + item.symbol
    : "0";
};

export const classNames = (...classes: string[]) => {
  return classes.filter(Boolean).join(" ");
};
