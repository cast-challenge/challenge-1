/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ['tailwindui.com', 'images.unsplash.com', 'picsum.photos', 'shuffle.dev'],
  },
  reactStrictMode: true,
  distDir: 'build'
}

module.exports = nextConfig
