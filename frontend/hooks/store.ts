import { configureStore } from '@reduxjs/toolkit'
import { addToCartSlice } from '../stores/addToCart'
import { consensysDemoApi } from '../stores/api'



export const store = configureStore({
  reducer: {
  
   addToCart: addToCartSlice.reducer,
   [consensysDemoApi.reducerPath]: consensysDemoApi.reducer,
//    authSlice: authSlice,
  },
  middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware().concat(consensysDemoApi.middleware),
})


export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch