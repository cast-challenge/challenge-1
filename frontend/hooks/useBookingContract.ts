import { Contract, ContractInterface, ethers } from "ethers";
import { useMemo, useState } from "react";
import { Connector, useAccount, useContract } from "wagmi";
import { ForgeBond, DVP, ERC20Token } from "../typechain";

export const BOOKING_ABI =
  require("./../ABI/contracts/tools/DVP.sol/DVP.json").abi;

export const useBookingContract = () => {
  const ADDRESS = process.env.DVP_CONTRACT_ADDRESS;

  const [{ data: accountData }, disconnect] = useAccount({
    fetchEns: false,
  });

  const [contract, setContract] = useState<Contract | undefined>();
  useMemo(async () => {
    if (!ADDRESS || !accountData?.connector) {
      return;
    }
    const signer = await accountData?.connector?.getSigner();
    setContract(new ethers.Contract(ADDRESS, BOOKING_ABI, signer));
  }, [ADDRESS, accountData?.connector]);

  return contract;
};
