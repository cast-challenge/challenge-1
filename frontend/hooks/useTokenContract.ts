import { Contract, ContractInterface, ethers } from "ethers";
import { useMemo, useState } from "react";
import { Connector, useAccount, useContract } from "wagmi";
import { ForgeBond, DVP, ERC20Token, ERC1400 } from "../typechain";

export const FORGE_ABI = require("./../deployments/localhost/ForgeBond.json");
export const ERC20_ABI = require("./../deployments/localhost/ERC20Token.json");
export const ERC1400_ABI = require("./../deployments/localhost/ERC1400.json");

export const useTokenContract = () => {
  const ADDRESS_FORGE = FORGE_ABI.address;
  const ADDRESS_ERC1400 = ERC1400_ABI.address;
  const ADDRESS_ERC20 = ERC20_ABI.address;

  const [{ data: accountData }, disconnect] = useAccount({
    fetchEns: false,
  });

  const [contractForge, setContractForge] = useState<ForgeBond | undefined>();
  const [contractERC20, setContractERC20] = useState<ERC20Token | undefined>();
  const [contractERC1400, setContractERC1400] = useState<ERC1400 | undefined>();

  useMemo(async () => {
    if (!ADDRESS_FORGE || !accountData?.connector) {
      return;
    }
    const signer = await accountData?.connector?.getSigner();
    setContractForge(
      new ethers.Contract(ADDRESS_FORGE, FORGE_ABI.abi, signer) as ForgeBond
    );
  }, [ADDRESS_FORGE, accountData?.connector]);

  useMemo(async () => {
    if (!ADDRESS_FORGE || !accountData?.connector) {
      return;
    }
    const signer = await accountData?.connector?.getSigner();
    setContractERC1400(
      new ethers.Contract(ADDRESS_ERC1400, ERC1400_ABI.abi, signer) as ERC1400
    );
  }, [ADDRESS_ERC1400, accountData?.connector]);

  useMemo(async () => {
    if (!ADDRESS_ERC20 || !accountData?.connector) {
      return;
    }
    const signer = await accountData?.connector?.getSigner();
    setContractERC20(
      new ethers.Contract(ADDRESS_ERC20, ERC20_ABI.abi, signer) as ERC20Token
    );
  }, [ADDRESS_FORGE, accountData?.connector]);

  return { contractForge, contractERC20, contractERC1400 };
};
