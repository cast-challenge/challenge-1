import {
  BaseContract,
  BytesLike,
  Contract,
  ContractInterface,
  utils,
  ethers,
} from "ethers";
import { useMemo, useState } from "react";
import { Connector, useAccount, useContract, useNetwork } from "wagmi";
import { Trade } from "../constants/tokens";
import { TradeRequest } from "../interfaces";
import { ForgeBond, DVP, ERC20Token } from "../typechain";

export const DVP_ABI = require("./../ABI/contracts/tools/DVP.sol/DVP.json").abi;
export const DVP_DEPLOYMENT = require("./../deployments/localhost/DVP.json");

export const useDVPContract = () => {
  const ADDRESS = DVP_DEPLOYMENT.address;

  const [{ data: accountData }, disconnect] = useAccount({
    fetchEns: false,
  });

  const [networkData] = useNetwork();
  const abiCoder = new utils.AbiCoder();
  const [contract, setContract] = useState<DVP | undefined>();
  const [trades, setTrades] = useState<Trade[] | undefined>();

  useMemo(async () => {
    if (!ADDRESS) {
      return;
    }
    console.log("networkData", networkData);

    const signer = await accountData?.connector?.getSigner();
    // console.log("useDVPContract", networkData, ADDRESS, signer);
    setContract(
      new ethers.Contract(ADDRESS, DVP_DEPLOYMENT.abi, signer) as DVP
    );
  }, [ADDRESS, accountData?.connector]);

  const getTrades = useMemo(async () => {
    if (!contract || !accountData?.address) {
      return [];
    }
    try {
      // console.log(!!contract?.deployed());
      contract?.deployed();
      // console.log("getTrades", contract?.address);
    } catch (error) {
      console.error(error);
      return;
    }
    const maxIndex = (await contract.getNbTrades()).toNumber();
    let index = 0;
    const tradesFormatted: Partial<Trade>[] = [];

    while (index < maxIndex) {
      const trade = await contract.getTrade(index);

      const formattedTrade: Trade = {
        holder1: trade[0].toString(),
        holder2: trade[1].toString(),
        executer: trade[2].toString(),
        expirationDate: trade[3].toString(),
        tokenData1Raw: trade[4].toString(),
        tokenData2Raw: trade[5].toString(),

        tradeType: trade[6] as number,
        state: trade[7] as number,
      };
      try {
        const dataToken2 = abiCoder.decode(
          ["address", "uint256", "bytes32", "uint256", "bool", "bool"],
          formattedTrade.tokenData2Raw
        );

        let partition2 = dataToken2[2].toString();
        partition2 = partition2.replace(/0+$/, "");
        console.log("partition2", partition2);

        const formattedTradeData2: TokenData = {
          address: dataToken2[0].toString(),
          amount: dataToken2[1].toString(),
          partition: ethers.utils.toUtf8String(partition2),
          standard: dataToken2[3].toString(),
          accepted: dataToken2[4],
          approved: dataToken2[5],
        };
        formattedTrade.tokenData2 = formattedTradeData2;

        const dataToken1 = abiCoder.decode(
          ["address", "uint256", "bytes32", "uint256", "bool", "bool"],
          formattedTrade.tokenData1Raw
        );
        let partition1 = dataToken1[2].toString();
        partition1 = partition1.replace(/0+$/, "");

        const formattedTradeData1: TokenData = {
          address: dataToken1[0].toString(),
          amount: dataToken1[1].toString(),
          partition: ethers.utils.toUtf8String(partition1),
          standard: dataToken1[3].toString(),
          accepted: dataToken1[4],
          approved: dataToken1[5],
        };

        formattedTrade.tokenData1 = formattedTradeData1;
        formattedTrade.tokenData2 = formattedTradeData2;
      } catch (error) {
        console.log(error);
      }

      index++;
      tradesFormatted.push(formattedTrade);
    }
    // console.log("tradesFormatted", tradesFormatted);
    setTrades(tradesFormatted as Trade[]);
    return tradesFormatted;
  }, [accountData?.address, contract, networkData?.name]);

  const requestTrade = useMemo(
    () => async (tradeRequest: TradeRequest) => {
      const trade = await contract?.requestTrade(tradeRequest);
      console.log(trade);
      trade?.wait().then(
        (res) => {
          console.log("trade done", res);
        },
        (err) => {
          console.log(err);
        }
      );
    },
    [contract]
  );
  const acceptTrade = useMemo(
    () => async (index: number) => {
      // const tradeIndex = await contract.
      const trade = await contract?.acceptTrade(index);
      console.log(trade);
      trade?.wait().then(
        (res) => {
          console.log("trade done", res);
        },
        (err) => {
          console.log(err);
        }
      );
    },
    [contract, accountData?.address]
  );
  const executeTrade = useMemo(
    () => async (index: number) => {
      // const tradeIndex = await contract.
      const trade = await contract?.executeTrade(index);
      console.log(trade);
      trade?.wait().then(
        (res) => {
          console.log("trade done", res);
        },
        (err) => {
          console.log(err);
        }
      );
    },
    [contract, accountData?.address]
  );
  const approveTrade = useMemo(
    () => async (index: number) => {
      // const tradeIndex = await contract.
      const trade = await contract?.approveTrade(index, true);
      console.log(trade);
      trade?.wait().then(
        (res) => {
          console.log("trade done", res);
        },
        (err) => {
          console.log(err);
        }
      );
    },
    [contract]
  );

  return {
    contract,
    trades,
    getTrades,
    requestTrade,
    acceptTrade,
    approveTrade,
    // approveTrade,
    executeTrade,
  };
};
