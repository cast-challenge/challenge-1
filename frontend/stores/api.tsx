// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery, retry } from '@reduxjs/toolkit/query/react'
import { RootState } from '../hooks/store';
import { BIAuthTokenRequest, BIAuthTokenResponse, Booking, IEvent, Room } from '../interfaces';

// Create our baseQuery instance
const baseQuery = fetchBaseQuery({
    baseUrl: process.env.NEXT_PUBLIC_API_URL,
    prepareHeaders: (headers, { getState }) => {
      // By default, if we have a token in the store, let's use that for authenticated requests
    //   const token = (getState() as RootState).authSlice.token;
    //   if (token) {
    //     headers.set('authentication', `Bearer ${token}`);
    //   }
      return headers;
    },
  });
  
  const baseQueryWithRetry = retry(baseQuery, { maxRetries: 6 });
  
// Define a service using a base URL and expected endpoints
export const consensysDemoApi = createApi({
    reducerPath: 'consensysDemoApi',
    baseQuery: baseQueryWithRetry,
    endpoints: (builder) => ({
        getAuthToken: builder.query<BIAuthTokenResponse, BIAuthTokenRequest>({
            query: (credentials: BIAuthTokenRequest) => ({ url: 'auth/init', method: 'POST', body: credentials, type: 'application/json'}),
        }),
        getEvents: builder.query<IEvent[], null>({
            query: () =>({ url:`events`, method: 'GET', type: 'application/json'}),
        }),
        getEvent: builder.query<IEvent, string>({
            query: (name) => `events/${name}`,
        }),
        getRooms: builder.query<Room[], null>({
            query: () =>({ url:`rooms`, method: 'GET', type: 'application/json'}),
        }),
        getRoom: builder.query<Room, string>({
            query: (id) => `room/${id}`,
        }),
        bookRoom: builder.query<Booking, Booking>({
            query: (booking: Booking) =>({ url:`bookings`, method: 'POST', type: 'application/json', body: booking}),
        }),
    }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { 
    useGetAuthTokenQuery, 
    useGetEventsQuery,
    useGetRoomsQuery 
} = consensysDemoApi

