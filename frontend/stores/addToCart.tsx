import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../hooks/store";
import { AddToCartState } from "../interfaces";

const SECONDS = 1000;

const initialState: AddToCartState = {
  offerId: "",
  timeStart: 0,
  timeEnd: 0,
  nbrHours: 1,
  isEmpty: true,
};

export const addToCartSlice = createSlice({
  name: "addToCartSlice",

  initialState,
  reducers: {
    incrementBookingNbrHours: (state) => {
      state.nbrHours += 1;
    },
    decrementBookingNbrHours: (state) => {
      state.nbrHours -= 1;
    },
    setBookingRoom: (state, action: PayloadAction<string>) => {
      state.roomId = action.payload;
      state.isEmpty = false;
    },

    setBookingTimeStart: (state, action: PayloadAction<number>) => {
      console.log(action?.payload);

      state.timeStart = action.payload;
      state.timeEnd =
        state.timeStart + state.timeStart * state.nbrHours * SECONDS;
      state.isEmpty = false;
    },

    setBookingDetails: (state, action: PayloadAction<AddToCartState>) => {
      state = action.payload;
    },
  },
});

export const {
  setBookingDetails,
  setBookingRoom,
  incrementBookingNbrHours,
  decrementBookingNbrHours,
  setBookingTimeStart,
} = addToCartSlice.actions;

export const currentAddToCartState = (state: RootState) => state.addToCart;

export default addToCartSlice.reducer;
