/* This example requires Tailwind CSS v2.0+ */
import { useEffect, useState } from "react";
import {
  CalendarIcon,
  LocationMarkerIcon,
  TrashIcon,
} from "@heroicons/react/solid";
import { Menu } from "@headlessui/react";
import { useNetwork, useAccount } from "wagmi";
import { useBookingContract } from "../hooks/useBookingContract";
import ErrorMessage from "./ErrorMessage";

import SmallCalendar from "./Calendar/SmallCalendar";
import Image from "next/image";
import { Booking } from "../interfaces";

const MyBookings = () => {
  const [userBookings, setUserBookings] = useState<Booking[]>([]);

  const dataContract = useBookingContract();

  const [
    { data: networkData, error: networkError, loading: networkLoading },
    switchNetwork,
  ] = useNetwork();

  const [{ data: accountData }, disconnect] = useAccount({
    fetchEns: true,
  });

  const removeBooking = async (index: number) => {
    await dataContract?.removeBooking(index);
    // TODO : use events to refresh bookings
    fetchUserBookings();
  };

  const updateUserBookings = (bookings: any[]) => {
    const userBookingsUpdated: Booking[] = bookings.map(
      (booking: any, index) => {
        const newDate = new Date(booking.timestamp.toNumber() * 1000);
        return {
          id: index,
          roomName: booking.room?.roomName,
          date: newDate.toLocaleDateString(),
          time: newDate.toLocaleTimeString(),
          dateTime: newDate.toLocaleString(),
        } as unknown as Booking;
      }
    );
    // console.log(' userBookingsUpdated', userBookingsUpdated);
    void setUserBookings(userBookingsUpdated);
  };

  const fetchUserBookings = () => {
    if (!networkData?.chain || !dataContract?.signer || !accountData) return;

    const userBookingsRaw: Promise<any> = dataContract?.getBookingByUser(
      accountData.address
    );
    userBookingsRaw.then((userBookingsRaw) => {
      updateUserBookings(userBookingsRaw);
    });
  };

  useEffect(() => {
    fetchUserBookings();
  }, [dataContract, accountData?.connector, accountData?.address]);

  return (
    <div>
      <h2 className="text-lg font-semibold text-gray-900">Upcoming meetings</h2>

      {accountData?.address && !userBookings?.length && (
        <div className="mt-4 divide-y divide-gray-100 text-sm leading-6 lg:col-span-7 xl:col-span-8">
          <ErrorMessage
            message="You have no upcoming meetings"
            title={"No upcoming meetings."}
          />
        </div>
      )}
      {accountData?.address ? (
        <div className="lg:grid lg:grid-cols-12 lg:gap-x-16">
          <div className="mt-10 text-center lg:col-start-8 lg:col-end-13 lg:row-start-1 lg:mt-9 xl:col-start-9">
            <SmallCalendar />
          </div>
          <ol className="mt-4 divide-y divide-gray-100 text-sm leading-6 lg:col-span-7 xl:col-span-8">
            {userBookings?.map((meeting) => (
              <li
                key={meeting.id}
                className="relative flex space-x-6 py-6 xl:static"
              >
                {meeting.imageUrl && (
                  <Image
                    src={meeting.imageUrl as string}
                    alt=""
                    className="h-14 w-14 flex-none rounded-full"
                  />
                )}
                <div className="flex-auto">
                  <h3 className="pr-10 font-semibold text-gray-900 xl:pr-0">
                    {meeting.roomName}
                  </h3>
                  <dl className="mt-2 flex flex-col text-gray-500 xl:flex-row">
                    <div className="flex items-start space-x-3">
                      <dt className="mt-0.5">
                        <span className="sr-only">Date</span>
                        <CalendarIcon
                          className="h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </dt>
                      <dd>
                        <time dateTime={meeting.datetime}>
                          {meeting.date} at {meeting.time}
                        </time>
                      </dd>
                    </div>
                    <div className="mt-2 flex items-start space-x-3 xl:mt-0 xl:ml-3.5 xl:border-l xl:border-gray-400 xl:border-opacity-50 xl:pl-3.5">
                      <dt className="mt-0.5">
                        <span className="sr-only">Location</span>
                        <LocationMarkerIcon
                          className="h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </dt>
                      <dd>{meeting.location}</dd>
                    </div>
                  </dl>
                </div>
                <Menu
                  as="div"
                  className="absolute top-6 right-0 xl:relative xl:top-auto xl:right-auto xl:self-center"
                >
                  <div>
                    <Menu.Button className="-m-2 flex items-center rounded-full p-2 text-gray-500 hover:text-gray-600">
                      <span className="sr-only">Open options</span>
                      <TrashIcon
                        className="h-5 w-5"
                        aria-hidden="true"
                        onClick={() => removeBooking(meeting.id)}
                      />
                    </Menu.Button>
                  </div>
                </Menu>
              </li>
            ))}
          </ol>
        </div>
      ) : (
        <ErrorMessage
          title={"Not connected"}
          message={"The Booking Page is accessible by signed in user."}
        />
      )}
    </div>
  );
};

export default MyBookings;
