import { useAccount, useConnect, useNetwork } from "wagmi";
import { truncateWallet } from "../utils/formatter";
import ConnectModal from "./ConnectModal";

const AccountMenu: React.FC = () => {
  const [{ data: connectData, error: connectError }, connect] = useConnect();
  const [{ data: accountData }, disconnect] = useAccount({
    fetchEns: true,
  });

  const [{ data: networkData, error, loading }, switchNetwork] = useNetwork();

  return (
    <div>
      {accountData ? (
        <div className="ml-10 space-x-4">
          <a
            href="#"
            className="inline-block bg-white-500 py-2 px-4 border border-transparent rounded-md text-base font-medium text-white hover:bg-opacity-75"
          >
            {networkData.chain?.name ?? networkData.chain?.id}{" "}
            {networkData.chain?.unsupported && "(unsupported)"}
          </a>
          <a
            href="#"
            className="inline-block bg-indigo-500 py-2 px-4 border border-transparent rounded-md text-base font-medium text-white hover:bg-opacity-75"
          >
            {truncateWallet(accountData?.address)}
          </a>
          <a
            onClick={disconnect}
            className="inline-block bg-white py-2 px-4 border border-transparent rounded-md text-base font-medium text-indigo-600 hover:bg-indigo-50"
          >
            Logout
          </a>
        </div>
      ) : (
        <div className="ml-10 space-x-4">
          <a
            href="#"
            onClick={() => connect(connectData.connectors[0])}
            className="inline-block bg-indigo-500 py-2 px-4 border border-transparent rounded-md text-base font-medium text-white hover:bg-opacity-75"
          >
            Sign in (Metamask)
          </a>
        </div>
      )}
    </div>
  );
};

export default AccountMenu;
