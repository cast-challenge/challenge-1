/* This example requires Tailwind CSS v2.0+ */

import Image from "next/image";
import Link from "next/link";
import AccountMenu from "./AccountMenu";

const navigation = [
  { name: "Home", href: "/" },
  { name: "Book", href: "/offers" },
  { name: "My reservations", href: "/my-bookings" },
  { name: "Admin", href: "/admin" },
];

export default function Header() {
  return (
    <header className="bg-indigo-600">
      <nav className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8" aria-label="Top">
        <div className="w-full py-6 flex items-center justify-between border-b border-indigo-500 lg:border-none">
          <div className="flex items-center">
            <span className="sr-only">Workflow</span>
            <div className="hidden ml-10 space-x-8 lg:block">
              {navigation.map((link) => (
                <Link href={link.href} key={link.name} passHref>
                  <a
                    key={link.name}
                    className="text-base font-medium text-white hover:text-indigo-50"
                  >
                    {link.name}
                  </a>
                </Link>
              ))}
            </div>
          </div>

          <AccountMenu></AccountMenu>
        </div>
        <div className="py-4 flex flex-wrap justify-center space-x-6 lg:hidden">
          {navigation.map((link) => (
            <Link href={link.href} key={link.name} passHref>
              <a
                key={link.name}
                className="text-base font-medium text-white hover:text-indigo-50"
              >
                {link.name}
              </a>
            </Link>
          ))}
        </div>
      </nav>
    </header>
  );
}
