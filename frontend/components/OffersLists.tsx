import { useEffect, useMemo, useState } from "react";
import { PARTITION_RESERVED } from "../constants/tokens";
// import { IProject, Listing } from "../../interfaces/interfaces";
// import { setCartDetails, setCartFromListing } from "../../stores/addToCart";
import { useAppDispatch, useAppSelector } from "../hooks";
import { useDVPContract } from "../hooks/useDVPContract";
import { IEvent, Offer, Room, TradeRequest } from "../interfaces";
import {
  currentAddToCartState,
  setBookingDetails,
  setBookingRoom,
} from "../stores/addToCart";
import { DVP } from "../typechain";
import TimeSelect from "./TimeSelect";

const OffersLists = (props: { offers: Offer[] }) => {
  const { offers } = props;
  const [currentOffer, setCurrentOffer] = useState<Offer | null>(null);
  const [DVPContractInstance, setDVPContract] = useState<DVP | null>(null);
  const [showConfirmationModal, setShowConfirmationModal] =
    useState<boolean>(false);

  const dispatch = useAppDispatch();
  const addToCartState = useAppSelector(currentAddToCartState);
  const { contract: DVPContract } = useDVPContract();

  const contract = useMemo(() => {
    if (DVPContractInstance) {
      return DVPContractInstance;
    }
  }, [DVPContractInstance]);

  const initiateTradeRequest = async () => {
    const tokenHolder = {
      address: process.env.NEXT_PUBLIC_SELLER_ACCOUNT_ADDRESS as string,
    };
    const settler = {
      address: process.env.NEXT_PUBLIC_SETTLER_ACCOUNT_ADDRESS as string,
    };
    const recipient = {
      address: process.env.NEXT_PUBLIC_BUYER_ACCOUNT_ADDRESS as string,
    };
    const contractBondInstance = {
      address: process.env.NEXT_PUBLIC_ERC1400_CONTRACT_ADDRESS as string,
    };
    const contractStableInstance = {
      address: process.env.NEXT_PUBLIC_ERC20_CONTRACT_ADDRESS as string,
    };
    // const contractDVPInstance = {
    //   address: ,
    // };
    const tradeRequestParams: TradeRequest = {
      holder1: tokenHolder.address,
      holder2: recipient.address,
      executer: settler.address,
      expirationDate: 1658798074,
      tokenAddress1: contractBondInstance.address,
      tokenValue1: 120,
      tokenId1: PARTITION_RESERVED,
      tokenStandard1: 5, // "0x45524331343030",
      tokenAddress2: contractStableInstance.address,
      tokenValue2: 360,
      tokenId2: PARTITION_RESERVED,
      tokenStandard2: 2, // "0x4552433230",
      tradeType: 1,
    };
    console.log("tradeRequestParams", tradeRequestParams);

    // if (currentOffer) {
    //   setShowConfirmationModal(true);
    // }
    const trade = await DVPContract?.requestTrade(tradeRequestParams);
    console.log(trade);
    trade?.wait().then((res) => {
      console.log("trade done", res);
    });
  };
  useMemo(async () => {
    // console.log("useMemo", DVPContract, await DVPContract?.getNbTrades());
    // const trade = (await DVPContract?.getTrade(1)) ?? null;
    // console.log("getTrade", trade, trade?.at(0));
    // console.log("getTradePrice", trade?.at(3)?.toString());
    // console.log("getTradePrice", (await DVPContract?.getPrice(1))?.toNumber());

    if (DVPContract?.address) {
      setDVPContract(DVPContract ?? null);
    }
  }, [DVPContract]);

  const getContractOwner = async () => {
    await contract?.owner();
  };

  // console.log(getContractOwner());

  return (
    <>
      {!offers?.length && (
        <section aria-labelledby="applicant-information-title">
          <div className="mt-0 max-w-lg mx-auto lg:mt-0 lg:max-w-none lg:mx-0 lg:col-start-3 lg:col-end-6 lg:row-start-1 lg:row-end-4">
            <div className={"relative z-10 rounded-lg"}>
              <div className="bg-white shadow sm:rounded-lg">
                <div className="px-4 py-5 sm:px-6">
                  <h2
                    id="applicant-information-title"
                    className="text-lg leading-6 font-medium text-gray-900 float-left"
                  >
                    Aucune offre
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
      {offers?.map((offer, index) => {
        const isSelected = addToCartState?.offerId === offer.id;
        return (
          <section key={index} aria-labelledby="applicant-information-title">
            <div className="mt-0 mb-10 max-w-lg mx-auto lg:max-w-none lg:mx-0 lg:col-start-3 lg:col-end-6 lg:row-start-1 lg:row-end-4">
              <div
                className={
                  isSelected
                    ? "relative z-10 rounded-lg shadow-xl"
                    : "relative z-10 rounded-lg"
                }
              >
                <div
                  className={
                    isSelected
                      ? "pointer-events-none absolute inset-0 rounded-lg border-2 border-indigo-600"
                      : ""
                  }
                  aria-hidden="true"
                />

                <div className="bg-white shadow sm:rounded-lg">
                  <div className="px-4 py-5 sm:px-6">
                    <h2
                      id="applicant-information-title"
                      className="text-lg leading-6 font-medium text-gray-900 float-left"
                    >
                      {offer.name}
                    </h2>
                  </div>
                  <div className="border-t border-gray-200 px-4 py-5 sm:px-6">
                    <dl className="grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2">
                      <div className="sm:col-span-1">
                        <dt className="text-sm font-medium text-gray-500">
                          Sponsorisé par
                        </dt>
                        <dd className="mt-1 text-sm text-gray-900">
                          {offer.organisation?.name}
                        </dd>
                      </div>
                      <div className="sm:col-span-1">
                        <dt className="text-sm font-medium text-gray-500">
                          Prix par heure
                        </dt>
                        <dd className="mt-1 text-sm text-gray-900">
                          {offer.price}€
                        </dd>
                      </div>

                      <div className="sm:col-span-2">
                        <dt className="text-sm font-medium text-gray-500">
                          Choisissez une offre
                        </dt>
                        <button onClick={initiateTradeRequest}>Payment</button>
                        {/* <TimeSelect roomId={room.id} /> */}
                      </div>
                    </dl>
                  </div>
                </div>
              </div>
            </div>
          </section>
        );
      })}
    </>
  );
};

export default OffersLists;
