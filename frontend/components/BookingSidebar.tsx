import moment from "moment";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../hooks";
import { useBookingContract } from "../hooks/useBookingContract";
import { AddToCartState } from "../interfaces";
import { currentAddToCartState } from "./../stores/addToCart";

import ConfirmationModal from "./Modals/ConfirmationModal";

const BookingSidebar = () => {
  // const { listing, updateCart, setBuySharesAmount, buySharesAmount, reserveShares } = props;
  const addToCartState = useAppSelector(currentAddToCartState);
  const dispatch = useAppDispatch();
  const [selectedListing, setSelectedListing] =
    useState<AddToCartState>(addToCartState);
  const [showConfirmationModal, setShowConfirmationModal] =
    useState<boolean>(true);

  const dataContract = useBookingContract();

  const bookRoom = async () => {
    /*
    @dev
    @param {number} _startTime
    @param {number} _roomHIndex
    @param {number} _roomHours
    */
    try {
      const res = await dataContract?.addBooking(
        addToCartState.timeStart,
        addToCartState.roomId,
        addToCartState.nbrHours
      );

      setShowConfirmationModal(true);
      res.wait().then((tx: any) => {
        console.log("tx", tx);
        setShowConfirmationModal(true);
        // dispatch(setBookingTimeStart(tx.blockNumber));
        // dispatch(setBookingRoom(tx.args.roomId));
      });
    } catch (error) {
      console.log("bookRoom error", error);
    }
  };

  useEffect(() => {
    setSelectedListing(addToCartState);
  }, [addToCartState]);
  return (
    <>
      <section
        aria-labelledby="timeline-title"
        className="lg:col-start-3 lg:col-span-1"
        //  fixed right-3
      >
        <div className="bg-white px-4 py-5 shadow sm:rounded-lg sm:px-6">
          <h2 id="timeline-title" className="text-lg font-medium text-gray-900">
            Effectuer une réservation
          </h2>
          <ConfirmationModal
            setOpen={setShowConfirmationModal}
            open={showConfirmationModal}
            title={"Votre réservation a été effectué"}
            description={"Retrouvez vos réservations dans Mon compte"}
            buttonText={"Fermer"}
          ></ConfirmationModal>
          {/* Activity Feed */}
          {!selectedListing.isEmpty && (
            <>
              <div className="mt-6 mb-4 flow-root">
                Vous pouvez verifier votre réservation
              </div>
              <dl className="mt-2 border-t border-b border-gray-200 divide-y divide-gray-200">
                <div className="py-3 flex justify-between text-sm font-medium">
                  <dt className="text-gray-500">Room</dt>
                  <dd className="text-gray-900">{selectedListing.roomId}</dd>
                </div>

                <div className="py-3 flex justify-between text-sm font-medium">
                  <dt className="text-gray-500">Date</dt>
                  <dd className="text-gray-900">
                    {moment(new Date(selectedListing.timeStart)).format(
                      "MMMM Do YYYY"
                    )}
                  </dd>
                </div>
                <div className="py-3 flex justify-between text-sm font-medium">
                  <dt className="text-gray-500">Heure</dt>
                  <dd className="text-gray-900">
                    {moment(new Date(selectedListing.timeStart)).format(
                      "h:mm:ss a"
                    )}
                    {}
                  </dd>
                </div>
              </dl>
            </>
          )}
          {!selectedListing && (
            <div className="mt-6 flow-root">
              Veuillez selectionner une offre pour continuer
            </div>
          )}
          <div className="mt-6 flex flex-col justify-stretch">
            <button
              onClick={() => {
                bookRoom();
              }}
              type="button"
              className="inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
            >
              Réserver
            </button>
            {/* {showConfirmationModal && (
              <>
              Transaction sent successfully</>
            )} */}
          </div>
        </div>
      </section>
    </>
  );
};

export default BookingSidebar;
