import Image from "next/image";
import Link from "next/link";

const Landing = () => {
  return (
    <>
      <section className="relative py-24 2xl:py-44 font-heading font-medium text-white bg-indigo-700 overflow-hidden rounded-t-10xl">
        <div className="relative container px-4 mx-auto z-10 text-center">
          <span className="inline-block mb-10 py-3 px-7 font-medium text-lg leading-5 border border-white rounded-6xl">
            Forge presents...
          </span>
          <h2 className="mb-14 xl:mb-16 font-heading text-9xl md:text-10xl xl:text-11xl leading-tight">
            Cast Challenge
          </h2>
          <p className="mb-14 xl:mb-16 font-sans font-normal text-base leading-6">
            Meet and Greet with the Cola Industry. No flavor added.
          </p>
          <Link href="/offers" passHref>
            <a
              className="text-black inline-block py-5 px-10 text-xl leading-6 font-medium tracking-tighter font-heading bg-white hover:bg-gray-50 text-body focus:ring-2 focus:ring-white focus:ring-opacity-50 rounded-xl"
              href="#"
            >
              View bulleting board
            </a>
          </Link>
        </div>
        <Image
          className="absolute top-0 left-1/2 transform -translate-x-1/2 md:px-6 h-full"
          src="https://shuffle.dev/uinel-assets/elements/call-to-action/elipse-bg-blue.svg"
          alt=""
          layout="fill"
        />
      </section>
      <section className="py-24 2xl:pt-40 2xl:pb-44 bg-white">
        <div className="container px-4 mx-auto">
          <div className="flex flex-wrap -mx-4">
            <div className="w-full lg:w-2/5 px-4 mb-20 lg:mb-0">
              <span className="block mb-9 text-xs font-medium uppercase leading-4 text-gray-300 tracking-widest">
                How it works
              </span>
              <h2 className="mb-12 text-9xl md:text-10xl xl:text-11xl font-medium font-heading leading-none">
                Easy to use. Really
              </h2>
              <p className="mb-16 lg:mb-20 text-xl lg:text-2xl leading-relaxed font-heading font-medium">
                Don&apos;t have Metamask ?
              </p>
              <div className="mb-12 font-heading">
                <button
                  className="py-5 px-10 mb-4 md:mb-0 mr-0 md:mr-5 w-full md:w-auto text-xl leading-none font-medium tracking-tighter font-heading bg-gray-50 hover:bg-gray-100 focus:ring-2 focus:ring-gray-50 focus:ring-opacity-50 rounded-xl"
                  type="button"
                >
                  Download
                </button>
                <button
                  className="py-5 px-10 w-full md:w-auto text-xl leading-none text-white font-medium tracking-tighter font-heading bg-blue-500 hover:bg-blue-600 focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50 rounded-xl"
                  type="button"
                >
                  Get started
                </button>
              </div>
              <p className="text-lg leading-6 text-darkBlueGray-400 font-normal">
                Explore the plaftorm right here.
              </p>
            </div>
            <div className="w-full lg:w-3/5 px-4">
              <div className="flex flex-wrap -mx-4 items-start">
                <div className="w-full md:w-1/2 px-4 mb-12 md:mb-0 md:mt-40 relative">
                  <div className="relative pt-16 pb-20 px-8 md:px-12 2xl:px-16 -mx-1 bg-white border border-black border-opacity-10 z-10 rounded-3xl">
                    <div className="relative inline-flex items-center justify-center mb-8 w-12 h-12 leading-6 text-white bg-blue-500 rounded-full">
                      <span className="text-2xl font-bold">1</span>
                    </div>
                    <h2 className="mb-14 text-7xl 2xl:text-8xl leading-tight font-medium font-heading">
                      Decentralized Security Exchange
                    </h2>
                    <p className="text-lg text-darkBlueGray-400">
                      The nulla commodo, commodo eros a, tristique lectus.
                    </p>
                  </div>
                  <div className="absolute left-1/2 -bottom-3 transform -translate-x-1/2 w-11/12 h-24 border border-black border-opacity-10 rounded-3xl"></div>
                </div>
                <div className="w-full md:w-1/2 px-4 relative">
                  <div className="relative pt-16 pb-20 px-8 md:px-12 2xl:px-16 -mx-1 bg-white border border-black border-opacity-10 z-10 rounded-3xl">
                    <div className="relative inline-flex items-center justify-center mb-8 w-12 h-12 leading-6 text-white bg-blue-500 rounded-full">
                      <span className="text-2xl font-bold">2</span>
                    </div>
                    <h2 className="mb-14 text-7xl 2xl:text-8xl leading-tight font-medium font-heading">
                      Manage your securities
                    </h2>
                    <p className="text-lg text-darkBlueGray-400">
                      Proin nec nunc felis. In non tellus ultricies, rutrum
                      lacus et, pharetra elit.
                    </p>
                  </div>
                  <div className="absolute left-1/2 -bottom-3 transform -translate-x-1/2 w-11/12 h-24 border border-black border-opacity-10 rounded-3xl"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default Landing;
