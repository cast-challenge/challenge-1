/* This example requires Tailwind CSS v2.0+ */
import { useCallback, useEffect, useMemo, useState } from "react";
import { RadioGroup } from "@headlessui/react";
import { classNames } from "../utils/formatter";
import { useAppSelector, useAppDispatch } from "../hooks";
import {
  currentAddToCartState,
  setBookingRoom,
  setBookingTimeStart,
} from "../stores/addToCart";
import moment from "moment";

interface TimeOption {
  name: string;
  inStock?: boolean;
  timestamp?: number;
}

const START_DATE = new Date("2022-04-08T08:00+0200");

const getTimestamp = (offset: number) => {
  return moment(START_DATE).add(offset, "h").toDate().getTime();
};
const timeOptions: TimeOption[] = [
  { name: "8h - 9h", inStock: true, timestamp: getTimestamp(0) },
  {
    name: "9h - 10h",
    inStock: true,
    timestamp: getTimestamp(1),
  },
  { name: "10h - 11h", inStock: true, timestamp: getTimestamp(2) },
  { name: "14h - 15h", inStock: true, timestamp: getTimestamp(6) },
  { name: "15h - 16h", inStock: true, timestamp: getTimestamp(7) },
  { name: "16h - 17h", inStock: false, timestamp: getTimestamp(8) },
];
interface Props {
  roomId: string;
}
const TimeSelect = (props: Props) => {
  const { roomId } = props;
  const [selectedTime, setSelectedTime] = useState<TimeOption | undefined>(
    undefined
  );
  const [isCurrentSelectedRoom, setIsCurrentSelectedRoom] = useState(false);
  const addToCartState = useAppSelector(currentAddToCartState);
  const dispatch = useAppDispatch();

  const selectTime = (start: number) => {
    dispatch(setBookingTimeStart(start));
  };

  useMemo(() => {
    console.log(" userBookingsUpdated", selectedTime, roomId);
    if (selectedTime && selectedTime?.timestamp) {
      dispatch(setBookingTimeStart(selectedTime?.timestamp));
      dispatch(setBookingRoom(roomId));
    }
  }, [selectedTime, roomId]);

  useEffect(() => {
    console.log("useEffect", addToCartState);
    if (addToCartState.isEmpty) return;
    if (roomId && addToCartState?.roomId) {
      const isCurrent = addToCartState?.roomId === roomId;
      console.log("isCurrent", isCurrent);

      setIsCurrentSelectedRoom(isCurrent);
    }
  }, [addToCartState.roomId]);

  return (
    <div>
      <RadioGroup
        value={selectedTime}
        onChange={setSelectedTime}
        className="mt-2"
      >
        <RadioGroup.Label className="sr-only">
          Choose a time option
        </RadioGroup.Label>
        <div className="grid grid-cols-3 gap-3 sm:grid-cols-6">
          {timeOptions.map((option) => (
            <RadioGroup.Option
              key={option.name}
              value={option}
              className={({ active, checked }) =>
                classNames(
                  option.inStock
                    ? "cursor-pointer focus:outline-none"
                    : "opacity-25 cursor-not-allowed",
                  active ? "ring-2 ring-offset-2 ring-indigo-500" : "",
                  isCurrentSelectedRoom && checked
                    ? "bg-indigo-600 border-transparent text-white hover:bg-indigo-700"
                    : "bg-white border-gray-200 text-gray-900 hover:bg-gray-50",
                  "border rounded-md py-3 px-3 flex items-center justify-center text-sm font-medium uppercase sm:flex-1"
                )
              }
              disabled={!option.inStock}
              onClick={() => {
                alert("ok");
                selectTime(option.timestamp as number);
              }}
            >
              <RadioGroup.Label as="p">{option.name}</RadioGroup.Label>
            </RadioGroup.Option>
          ))}
        </div>
      </RadioGroup>
    </div>
  );
};
export default TimeSelect;
