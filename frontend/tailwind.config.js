module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      borderColor: (theme) => ({
        ...theme('colors'),
        primary: '#C0E21B',
        secondary: '#357F9C',
        frenchGrey: '#B9B8BA',
        error: '#D42338',
        input: '#DFE1E6',
      }),
      backgroundColor: {
        lightGray: '#F9FAFA',
        fieldError: '#FFF7FA',
        body: '#F1F5FB',
        dark: '#171925',
      },
      textColor: {
        error: '#D42338',
      },
      minWidth: {
        datePicker: '200px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/aspect-ratio')
  ],
};
