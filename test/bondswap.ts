import { expect } from "chai";
import { ethers } from "hardhat";
import { ForgeBond } from "../typechain";
// import { ethers } from "harhat";
export type BigNumberish = bigint | string | number;

interface ContractInput {
  initialSupply: BigNumberish;
  isinCode: string;
  name: string;
  symbol: string;
  denomination: BigNumberish;
  divisor: BigNumberish;
  startDate: BigNumberish;
  initialMaturityDate: BigNumberish;
  firstCouponDate: BigNumberish;
  couponFrequencyInMonths: BigNumberish;
  interestRateInBips: BigNumberish;
  callable: boolean;
  isSoftBullet: boolean;
  softBulletPeriodInMonths: BigNumberish;
  currency: string;
  registrar: string;
  settler: string;
  owner: string;
}
const args: ContractInput = {
  initialSupply: "100000000000000000000",
  isinCode: "US0378331005",
  name: "BondSwap",
  symbol: "BS",
  denomination: "100000000000000000000",
  divisor: "100000000000000000000",
  startDate: "0",
  initialMaturityDate: "0",
  firstCouponDate: "0",
  couponFrequencyInMonths: "0",
  interestRateInBips: "0",
  callable: true,
  isSoftBullet: false,
  softBulletPeriodInMonths: "0",
  currency: "0x0000000000000000000000000000000000000000",
  registrar: "0x0000000000000000000000000000000000000000",
  settler: "0x0000000000000000000000000000000000000000",
  owner: "0x0000000000000000000000000000000000000000",
};
describe("ForgeBond", function () {
  let contractInstance: ForgeBond;

  beforeEach(async () => {
    const signers = await ethers.getSigners();

    const LibOperatorManagerLibrary = await ethers.getContractFactory(
      "OperatorManagerLibrary"
    );
    const libOperatorManagerLibrary = await LibOperatorManagerLibrary.deploy();
    await libOperatorManagerLibrary.deployed();

    const LibIterableBalances = await ethers.getContractFactory(
      "IterableBalances"
    );
    const libIterableBalances = await LibIterableBalances.deploy();
    await libIterableBalances.deployed();

    const LibSecurityTokenBalancesLibrary = await ethers.getContractFactory(
      "SecurityTokenBalancesLibrary",
      {
        signer: signers[0],
        libraries: {
          IterableBalances: libIterableBalances.address,
        },
      }
    );
    const libSecurityTokenBalancesLibrary =
      await LibSecurityTokenBalancesLibrary.deploy();
    await libSecurityTokenBalancesLibrary.deployed();

    const LibSettlementRepositoryLibrary = await ethers.getContractFactory(
      "SettlementRepositoryLibrary"
    );
    const libSettlementRepositoryLibrary =
      await LibSettlementRepositoryLibrary.deploy();
    await libSettlementRepositoryLibrary.deployed();

    const LibSettlementWorkflowLibrary = await ethers.getContractFactory(
      "SettlementWorkflowLibrary",
      {
        signer: signers[0],
        libraries: {
          SecurityTokenBalancesLibrary: libSecurityTokenBalancesLibrary.address,
          SettlementRepositoryLibrary: libSettlementRepositoryLibrary.address,
        },
      }
    );
    const libSettlementWorkflowLibrary =
      await LibSettlementWorkflowLibrary.deploy();
    await libSettlementWorkflowLibrary.deployed();

    const contractFactory = await ethers.getContractFactory("ForgeBond", {
      signer: signers[0],
      libraries: {
        OperatorManagerLibrary: libOperatorManagerLibrary.address,
        SettlementWorkflowLibrary: libSettlementWorkflowLibrary.address,
        SettlementRepositoryLibrary: libSettlementRepositoryLibrary.address,
        SecurityTokenBalancesLibrary: libSecurityTokenBalancesLibrary.address,
      },
    });

    contractInstance = await contractFactory
      .deploy(args)
      .then((contractFactory) => contractFactory.deployed());
  });

  it("Should return the contract symbol", async function () {
    expect(await contractInstance.symbol()).to.equal("BS");
  });
});
