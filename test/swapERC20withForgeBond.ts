import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect } from "chai";
import { BytesLike } from "ethers";
import { ethers, getNamedAccounts } from "hardhat";
import { ForgeBond, DVP, ERC20Token } from "../typechain";
import { PARTITION_RESERVED } from "../constants/constants";
import * as constantsForge from "../constants/forge";
// import { ethers } from "harhat";
const settlementTransactionId1 = "100000000000000000000000000000000000001";
const settlementTransactionId2 = "200000000000000000000000000000000000002";
const settlementTransactionId3 = "300000000000000000000000000000000000003";

const TOKEN_LOCKED = 2;
const CASH_RECEIVED = 3;
const CASH_SENT = 4;
const CANCELED = 5;
const SUBSCRIPTION = 0x1;
const operationId = 0xdab;
const txHash = "0xABBA";

const fromRegistrarTransactionDetails = { from: constantsForge.registrar };

const fromSettlerTransactionDetails = { from: constantsForge.settler };

// eslint-disable-next-line @typescript-eslint/no-var-requires
const Web3 = require("web3");

// const ERC1820Registry = artifacts.require("IERC1820Registry");
const {
  BN, // Big Number support
  constants, // Common constants, like the zero address and largest integers
  expectEvent, // Assertions for emitted events
  assertEvent,
  assertEventArgs,
  expectRevert, // Assertions for transactions that should fail
  singletons, // Singletons for common types, like the zero address
} = require("@openzeppelin/test-helpers");

export type BigNumberish = bigint | string | number;

interface ContractInput {
  initialSupply: BigNumberish;
  isinCode: string;
  name: string;
  symbol: string;
  denomination: BigNumberish;
  divisor: BigNumberish;
  startDate: BigNumberish;
  initialMaturityDate: BigNumberish;
  firstCouponDate: BigNumberish;
  couponFrequencyInMonths: BigNumberish;
  interestRateInBips: BigNumberish;
  callable: boolean;
  isSoftBullet: boolean;
  softBulletPeriodInMonths: BigNumberish;
  currency: string;
  registrar: string;
  settler: string;
  owner: string;
}

export interface TradeRequest {
  holder1: string;
  holder2: string;
  executer: string;
  expirationDate: BigNumberish;
  tokenAddress1: string;
  tokenValue1: BigNumberish;
  tokenId1: BytesLike;
  tokenStandard1: BigNumberish;
  tokenAddress2: string;
  tokenValue2: BigNumberish;
  tokenId2: BytesLike;
  tokenStandard2: BigNumberish;
  tradeType: BigNumberish;
}
interface ERC20ContractInput {
  name: string;
  symbol: string;
  denomination: BigNumberish;
}

const args: ContractInput = {
  initialSupply: "100000000",
  isinCode: "US0378331005",
  name: "BondSwap",
  symbol: "REF",
  denomination: "1000",
  divisor: "100",
  startDate: "0",
  initialMaturityDate: "0",
  firstCouponDate: "0",
  couponFrequencyInMonths: "0",
  interestRateInBips: "0",
  callable: true,
  isSoftBullet: false,
  softBulletPeriodInMonths: "0",
  currency: "0x0000000000000000000000000000000000000000",
  registrar: "0x0000000000000000000000000000000000000000",
  settler: "0x0000000000000000000000000000000000000000",
  owner: "0x0000000000000000000000000000000000000000",
};
describe("ForgeBond", function () {
  let contractBondInstance: ForgeBond;
  let contractStableInstance: ERC20Token;
  let contractDVPInstance: DVP;
  let deployerAddress: string;
  let contractArgs: ContractInput;

  let owner: SignerWithAddress;
  let controller: SignerWithAddress;
  let deployer: SignerWithAddress;
  let tokenHolder: SignerWithAddress;
  let recipient: SignerWithAddress;
  let unknown: SignerWithAddress;
  let operator: SignerWithAddress;
  let settler: SignerWithAddress;

  before(async () => {
    const accounts = await ethers.getSigners();
    const namedAccounts = await getNamedAccounts();
    deployer = accounts[0];
    owner = accounts[1];
    controller = accounts[2];
    tokenHolder = accounts[3];
    recipient = accounts[4];
    unknown = accounts[5];
    operator = accounts[6];
    settler = accounts[7];
    await singletons.ERC1820Registry(deployer.address);

    contractArgs = {
      ...args,
      owner: deployer.address,
      settler: settler.address,
      registrar: settler.address,
    };
    const paramsStable: ERC20ContractInput = {
      name: "Refundia Stable",
      symbol: "STB",
      denomination: "100",
    };

    const DEPLOYER_ADDRESS = deployer.address;
    deployerAddress = DEPLOYER_ADDRESS;
    const LibOperatorManagerLibrary = await ethers.getContractFactory(
      "OperatorManagerLibrary"
    );
    const libOperatorManagerLibrary = await LibOperatorManagerLibrary.deploy();
    await libOperatorManagerLibrary.deployed();

    const LibIterableBalances = await ethers.getContractFactory(
      "IterableBalances"
    );
    const libIterableBalances = await LibIterableBalances.deploy();
    await libIterableBalances.deployed();

    const LibSecurityTokenBalancesLibrary = await ethers.getContractFactory(
      "SecurityTokenBalancesLibrary",
      {
        // signer: deployer,
        libraries: {
          IterableBalances: libIterableBalances.address,
        },
      }
    );
    const libSecurityTokenBalancesLibrary =
      await LibSecurityTokenBalancesLibrary.deploy();
    await libSecurityTokenBalancesLibrary.deployed();

    const LibSettlementRepositoryLibrary = await ethers.getContractFactory(
      "SettlementRepositoryLibrary"
    );
    const libSettlementRepositoryLibrary =
      await LibSettlementRepositoryLibrary.deploy();
    await libSettlementRepositoryLibrary.deployed();

    const LibSettlementWorkflowLibrary = await ethers.getContractFactory(
      "SettlementWorkflowLibrary",
      {
        // signer: deployer,
        libraries: {
          SecurityTokenBalancesLibrary: libSecurityTokenBalancesLibrary.address,
          SettlementRepositoryLibrary: libSettlementRepositoryLibrary.address,
        },
      }
    );
    const libSettlementWorkflowLibrary =
      await LibSettlementWorkflowLibrary.deploy();
    await libSettlementWorkflowLibrary.deployed();

    const contractFactory = await ethers.getContractFactory("ForgeBond", {
      //   signer: deployer,
      libraries: {
        OperatorManagerLibrary: libOperatorManagerLibrary.address,
        SettlementWorkflowLibrary: libSettlementWorkflowLibrary.address,
        SettlementRepositoryLibrary: libSettlementRepositoryLibrary.address,
        SecurityTokenBalancesLibrary: libSecurityTokenBalancesLibrary.address,
      },
    });

    const contract20Factory = await ethers.getContractFactory("ERC20Token", {
      // signer: deployer,
    });

    const contractDVPFactory = await ethers.getContractFactory("DVP", {
      // signer: deployer,
    });

    contractBondInstance = await contractFactory
      .deploy(contractArgs)
      .then((contractFactory) => contractFactory.deployed());

    contractDVPInstance = await contractDVPFactory
      .deploy(true, false)
      .then((contractFactory) => contractFactory.deployed());

    contractStableInstance = await contract20Factory
      .deploy(paramsStable.name, paramsStable.symbol, paramsStable.denomination)
      .then((contractFactory) => contractFactory.deployed());
  });

  it("Should return the contract symbol", async function () {
    expect(await contractBondInstance.symbol()).to.equal("REF");
    expect(await contractStableInstance.symbol()).to.equal("STB");
  });

  it("Should be properly initialized", async function () {
    expect(await contractDVPInstance.getNbTrades()).to.equal(0);
    // console.log(await contractDVPInstance.tradeExecuters());

    expect(await contractDVPInstance.tradeExecuters()).to.eql(
      [deployerAddress],
      '"tradeExecuters" should be equal to "0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266"'
    );
    await contractDVPInstance.setTradeExecuters([settler.address]);
    expect(await contractDVPInstance.tradeExecuters()).to.eql([
      settler.address,
    ]);
  });

  it("Should distribute tokens", async function () {
    expect(await contractBondInstance.balanceOf(deployer.address)).to.equal(
      100000000
    );
    expect(await contractBondInstance.balanceOf(recipient.address)).to.equal(0);
    await (
      await contractStableInstance
        .connect(deployer)
        .mint(recipient.address, 50000)
    ).wait();
    expect(await contractStableInstance.balanceOf(recipient.address)).to.equal(
      50000
    );
  });

  it("Should set token controller for Stable coin", async function () {
    expect(
      await contractDVPInstance.tokenControllers(contractStableInstance.address)
    ).to.eql([]);
    await contractDVPInstance.setTokenControllers(
      contractStableInstance.address,
      [controller.address]
    );
    expect(
      await contractDVPInstance.tokenControllers(contractStableInstance.address)
    ).to.eql([controller.address]);
  });

  it("Should set token controller for Bond", async function () {
    expect(
      await contractDVPInstance.tokenControllers(contractBondInstance.address)
    ).to.eql([]);

    await contractDVPInstance.setTokenControllers(
      contractBondInstance.address,
      [deployer.address]
    );
    expect(
      await contractDVPInstance.tokenControllers(contractBondInstance.address)
    ).to.eql([deployer.address]);
  });

  it("Should set price oracle", async function () {
    expect(
      await contractDVPInstance.priceOracles(contractBondInstance.address)
    ).to.eql([]);

    await contractDVPInstance.setPriceOracles(contractBondInstance.address, [
      deployer.address,
    ]);
    expect(
      await contractDVPInstance.priceOracles(contractBondInstance.address)
    ).to.eql([deployer.address]);
  });

  it("Should set price for pair", async function () {
    try {
      expectRevert(
        await contractDVPInstance.setTokenPrice(
          contractBondInstance.address,
          contractStableInstance.address,
          PARTITION_RESERVED,
          PARTITION_RESERVED,
          "88888"
        )
      );
    } catch (error) {
      //   console.log(error);
    }
    expect(
      await contractDVPInstance.getTokenPrice(
        contractBondInstance.address,
        contractStableInstance.address,
        PARTITION_RESERVED,
        PARTITION_RESERVED
      )
    ).to.equal(0);

    await contractDVPInstance.setPriceOwnership(
      contractBondInstance.address,
      contractStableInstance.address,
      true
    );

    await contractDVPInstance.setTokenPrice(
      contractBondInstance.address,
      contractStableInstance.address,
      PARTITION_RESERVED,
      PARTITION_RESERVED,
      "88888"
    );

    expect(
      await contractDVPInstance.getTokenPrice(
        contractBondInstance.address,
        contractStableInstance.address,
        PARTITION_RESERVED,
        PARTITION_RESERVED
      )
    ).to.equal(88888);
  });
  it("Should set allowance", async function () {
    // enum Standard {OffChain, ETH, ERC20, ERC721, ERC1400, ForgeBond}
    // const deployer = "0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266";
    await (
      await contractBondInstance.authorizeOperator(
        "1",
        contractDVPInstance.address
      )
    ).wait();
    await (
      await contractBondInstance.authorizeOperator(
        "2",
        contractDVPInstance.address
      )
    ).wait();

    // const txHolder1 = await contractBondInstance
    //   .connect(tokenHolder)
    //   .isOperatorWithRoleAuthorized(contractDVPInstance.address, "1");

    expect(
      await contractBondInstance.isOperatorWithRoleAuthorized(
        contractDVPInstance.address,
        "1"
      )
    ).to.equal(true);
    expect(
      await contractBondInstance.isOperatorWithRoleAuthorized(
        contractDVPInstance.address,
        "2"
      )
    ).to.equal(true);

    const txHolder2 = await contractStableInstance
      .connect(recipient)
      .increaseAllowance(contractDVPInstance.address, 4000000000000000);
    await txHolder2.wait();

    expect(
      await contractStableInstance.allowance(
        recipient.address,
        contractDVPInstance.address
      )
    ).to.equal(4000000000000000);
  });

  //   it("Should fail transfer bond token", async function () {
  //     const AMOUNT_TO_TRANSFER = 1000;
  //     const txHolder1 = await contractBondInstance
  //       .connect(deployer)
  //       .transfer(recipient.address, AMOUNT_TO_TRANSFER);
  //     // await txHolder1.wait();

  //     // await contractBondInstance.to
  //     expect(await contractBondInstance.balanceOf(deployer.address)).to.equal(
  //       parseInt(contractArgs.initialSupply.toString()) - AMOUNT_TO_TRANSFER
  //     );
  //     expect(await contractBondInstance.balanceOf(recipient.address)).to.equal(
  //       AMOUNT_TO_TRANSFER
  //     );
  //   });

  it("should issue bond token to tokenHolder", async function () {
    const AMOUNT_TO_TRANSFER = 1000;

    const settlementTransaction1 = {
      txId: settlementTransactionId1,
      operationId,
      deliverySenderAccountNumber: deployer.address,
      deliveryReceiverAccountNumber: tokenHolder.address,
      deliveryQuantity: AMOUNT_TO_TRANSFER,
      txHash,
    };
    // const settlementTransaction2 = {
    //   txId: settlementTransactionId2,
    //   operationId,
    //   deliverySenderAccountNumber: constants.owner,
    //   deliveryReceiverAccountNumber: constants.investor2Address,
    //   deliveryQuantity: 23,
    //   txHash,
    // };

    await (
      await contractBondInstance.connect(settler).initiateSubscription(
        settlementTransaction1
        //   fromRegistrarTransactionDetails
      )
    ).wait();

    await (
      await contractBondInstance
        .connect(settler)
        .confirmPaymentReceived(settlementTransactionId1)
    ).wait();

    expect(await contractBondInstance.balanceOf(tokenHolder.address)).to.equal(
      AMOUNT_TO_TRANSFER
    );

    // return;
    // await contractBondInstance
    //   .initiateSubscription(
    //     settlementTransaction2,
    //     fromRegistrarTransactionDetails
    //   )
    //   .then((res) => {
    //     const index = assertEvent(res, "SubscriptionInitiated");
    //     assertEventArgs(
    //       res,
    //       index,
    //       "settlementTransactionId",
    //       settlementTransactionId2
    //     );
    //   })
    //   .then(
    //     async () =>
    //       await contractBondInstance.getCurrentState(settlementTransactionId1)
    //   );
    await contractBondInstance.getOperationType(operationId).then((res) => {
      //   expect.fail(
      //     res.toNumber(),
      //     SUBSCRIPTION,
      //     "operationType store in the smartContract should be SUBSCRIPTION"
      //   );
    });
  });

  it("Should set a trade", async function () {
    // enum Standard {OffChain, ETH, ERC20, ERC721, ERC1400, ForgeBond}

    // enum State {Undefined, Pending, Executed, Forced, Cancelled}

    // enum TradeType {Escrow, Swap}

    // enum Holder {Holder1, Holder2}

    const trade: TradeRequest = {
      holder1: tokenHolder.address,
      holder2: recipient.address,
      executer: settler.address,
      expirationDate: 1658798074,
      tokenAddress1: contractBondInstance.address,
      tokenValue1: 100,
      tokenId1: PARTITION_RESERVED,
      tokenStandard1: 5, // "0x45524331343030",
      tokenAddress2: contractStableInstance.address,
      tokenValue2: 350,
      tokenId2: PARTITION_RESERVED,
      tokenStandard2: 2, // "0x4552433230",
      tradeType: 1,
    };

    const tradeResponse: any = [
      trade.holder1,
      trade.holder2,
      trade.executer,
      {
        _hex: "0x62df3ffa",
        _isBigNumber: true,
      },
      trade.tokenId1,
      trade.tokenId2,
      trade.tradeType,
      1,
    ];

    // await contractD
    await contractDVPInstance.setTradeExecuters([settler.address]);

    // return;
    const newTrade = await (
      await contractDVPInstance.connect(deployer).requestTrade(trade)
    ).wait();
    // console.log(newTrade);

    expect(await contractDVPInstance.getNbTrades()).to.equal(1);

    expect((await contractDVPInstance.getTrade(1))[0]).to.eql(tradeResponse[0]);

    expect(await contractDVPInstance.getPrice(1)).to.equal(350);
    await (
      await contractDVPInstance.connect(tokenHolder).acceptTrade(1)
    ).wait();

    await (await contractDVPInstance.connect(recipient).acceptTrade(1)).wait();
    // await (await contractDVPInstance.connect(controller).acceptTrade(1)).wait();

    // console.log(await contractDVPInstance.getTradeAcceptanceStatus(1));
    // console.log(await contractDVPInstance.getTradeApprovalStatus(1));
    // console.log(

    await contractDVPInstance.connect(deployer).approveTrade(1, true);
    await contractDVPInstance.connect(controller).approveTrade(1, true);
    // );

    expect(await contractBondInstance.balanceOf(tokenHolder.address)).to.equal(
      1000
    );
    expect(await contractBondInstance.balanceOf(recipient.address)).to.equal(0);

    expect(
      await contractStableInstance.balanceOf(tokenHolder.address)
    ).to.equal(0);
    expect(await contractStableInstance.balanceOf(recipient.address)).to.equal(
      50000
    );

    await (await contractDVPInstance.connect(settler).executeTrade(1)).wait();
    // console.log(await contractDVPInstance.getTradeAcceptanceStatus(1));
    // console.log(await contractDVPInstance.getTradeApprovalStatus(1));

    expect(await contractBondInstance.balanceOf(tokenHolder.address)).to.equal(
      900
    );
    expect(await contractBondInstance.balanceOf(recipient.address)).to.equal(
      100
    );

    expect(
      await contractStableInstance.balanceOf(tokenHolder.address)
    ).to.equal(350);
    expect(await contractStableInstance.balanceOf(recipient.address)).to.equal(
      49650
    );

    // console.log(
    const balances = (await contractBondInstance.getFullBalances()).map(
      async (res: any) => {
        const balance = await contractStableInstance.balanceOf(res[0]);
        const str = `Balance of ${res[0]} : ${
          res[1]
        } BondUnits an and ${balance.toNumber()} StableUnits`;
        // console.log(str);
        return str;
      }
    );
    // );
    balances.forEach((res) => {
      res.then((str) => {
        console.log("\r\nHello");
      });
    });
  });
});
