import { expect } from "chai";
import { artifacts, ethers, getNamedAccounts } from "hardhat";

import {
  DEFAULT_PARTITIONS,
  INITIAL_SUPPLY,
  PARTITION_ISSUED,
  PARTITION_RESERVED,
  ZERO_BYTES32,
} from "../constants/constants";
import { DVP, ERC20Token } from "../typechain";

// import ERC20TOKEN from "../artifacts/contracts/tokens/ERC20Token.sol/ERC20Token.json";
import { Artifact } from "hardhat/types";
import { BigNumberish } from "ethers";
import { AccessListish, BytesLike } from "ethers/lib/utils";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
// const ERC1820Registry = artifacts.require("IERC1820Registry");
const {
  BN, // Big Number support
  constants, // Common constants, like the zero address and largest integers
  expectEvent, // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
  singletons, // Singletons for common types, like the zero address
} = require("@openzeppelin/test-helpers");

export interface Overrides {
  gasLimit?: BigNumberish | Promise<BigNumberish>;
  gasPrice?: BigNumberish | Promise<BigNumberish>;
  maxFeePerGas?: BigNumberish | Promise<BigNumberish>;
  maxPriorityFeePerGas?: BigNumberish | Promise<BigNumberish>;
  nonce?: BigNumberish | Promise<BigNumberish>;
  type?: number;
  accessList?: AccessListish;
  customData?: Record<string, any>;
  ccipReadEnabled?: boolean;
}

export interface TradeRequest {
  holder1: string;
  holder2: string;
  executer: string;
  expirationDate: BigNumberish;
  tokenAddress1: string;
  tokenValue1: BigNumberish;
  tokenId1: BytesLike;
  tokenStandard1: BigNumberish;
  tokenAddress2: string;
  tokenValue2: BigNumberish;
  tokenId2: BytesLike;
  tokenStandard2: BigNumberish;
  tradeType: BigNumberish;
}

interface ContractInput {
  name: string;
  symbol: string;
  granularity: BigNumberish;
  controllers: string[];
  defaultPartitions: BytesLike[];
  overrides?: Overrides & { from?: string | Promise<string> };
}
const args: ContractInput = {
  name: "BondSwap",
  symbol: "BS",
  granularity: "100000000000000000000",
  controllers: ["0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266"],
  defaultPartitions: [
    "0x7265736572766564000000000000000000000000000000000000000000000000",
  ],
};

describe("ERC20", async function () {
  let contractBondInstance: ERC20Token;
  let contractStableInstance: ERC20Token;
  let contractDVPInstance: DVP;
  let deployerAddress: string;

  let owner: SignerWithAddress;
  let controller: SignerWithAddress;
  let deployer: SignerWithAddress;
  let tokenHolder: SignerWithAddress;
  let recipient: SignerWithAddress;
  let unknown: SignerWithAddress;
  let operator: SignerWithAddress;
  let settler: SignerWithAddress;

  before(async () => {
    const accounts = await ethers.getSigners();
    const namedAccounts = await getNamedAccounts();
    deployer = accounts[0];
    owner = accounts[1];
    controller = accounts[2];
    tokenHolder = accounts[3];
    recipient = accounts[4];
    unknown = accounts[5];
    operator = accounts[6];
    settler = accounts[7];

    const DEPLOYER_ADDRESS = deployer.address;
    deployerAddress = DEPLOYER_ADDRESS;

    await singletons.ERC1820Registry(DEPLOYER_ADDRESS);

    const params: ContractInput = {
      name: "Refundia Bond",
      symbol: "RFD",
      granularity: 18,
      controllers: [],
      defaultPartitions: DEFAULT_PARTITIONS,
    };

    const paramsStable: ContractInput = {
      name: "Refundia Stable",
      symbol: "STB",
      granularity: 18,
      controllers: [],
      defaultPartitions: DEFAULT_PARTITIONS,
    };

    const contractFactory = await ethers.getContractFactory("ERC20Token", {
      // signer: deployer,
    });

    const contractDVPFactory = await ethers.getContractFactory("DVP", {
      // signer: deployer,
    });

    console.log("Deploying contracts with the account:", DEPLOYER_ADDRESS);

    console.log("Account balance:" + (await deployer.getBalance()).toString());

    contractDVPInstance = await contractDVPFactory
      .deploy(true, false)
      .then((contractFactory) => contractFactory.deployed());

    contractBondInstance = await contractFactory
      .deploy(params.name, params.symbol, params.granularity)
      .then((contractFactory) => contractFactory.deployed());

    contractStableInstance = await contractFactory
      .deploy(paramsStable.name, paramsStable.symbol, paramsStable.granularity)
      .then((contractFactory) => contractFactory.deployed());

    console.log("Bond address:", contractBondInstance.address);
    console.log("Stable address:", contractStableInstance.address);
    console.log("DVP address:", contractDVPInstance.address);
  });
  it("Should return the new symbol again once it's changed", async function () {
    expect(await contractBondInstance.symbol()).to.equal("RFD");
    expect(await contractStableInstance.symbol()).to.equal("STB");

    // const setGreetingTx = await greeter.("Hola, mundo!");
    // // wait until the transaction is mined
    // await setGreetingTx.wait();
    // expect(await greeter.greet()).to.equal("Hola, mundo!");
  });

  it("Should be properly initialized", async function () {
    expect(await contractDVPInstance.getNbTrades()).to.equal(0);
    // console.log(await contractDVPInstance.tradeExecuters());

    expect(await contractDVPInstance.tradeExecuters()).to.eql(
      [deployerAddress],
      '"tradeExecuters" should be equal to "0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266"'
    );
    await contractDVPInstance.setTradeExecuters([settler.address]);
    expect(await contractDVPInstance.tradeExecuters()).to.eql([
      settler.address,
    ]);
  });

  it("Should distribute tokens", async function () {
    expect(await contractBondInstance.balanceOf(tokenHolder.address)).to.equal(
      0
    );
    await (
      await contractBondInstance
        .connect(deployer)
        .mint(tokenHolder.address, 20000)
    ).wait();
    expect(await contractBondInstance.balanceOf(tokenHolder.address)).to.equal(
      20000
    );

    expect(await contractBondInstance.balanceOf(recipient.address)).to.equal(0);
    await (
      await contractStableInstance
        .connect(deployer)
        .mint(recipient.address, 50000)
    ).wait();
    expect(await contractStableInstance.balanceOf(recipient.address)).to.equal(
      50000
    );
    // await contractBondInstance.transfer(tokenHolder, "4");
    // expect(await contractBondInstance.balanceOf(tokenHolder)).to.equal(4);
  });

  it("Should set token controller", async function () {
    expect(
      await contractDVPInstance.tokenControllers(contractBondInstance.address)
    ).to.eql([]);
    await contractDVPInstance.setTokenControllers(
      contractBondInstance.address,
      [controller.address]
    );
    expect(
      await contractDVPInstance.tokenControllers(contractBondInstance.address)
    ).to.eql([controller.address]);

    expect(
      await contractDVPInstance.tokenControllers(contractStableInstance.address)
    ).to.eql([]);
    await contractDVPInstance.setTokenControllers(
      contractStableInstance.address,
      [controller.address]
    );
    expect(
      await contractDVPInstance.tokenControllers(contractStableInstance.address)
    ).to.eql([controller.address]);
  });

  it("Should set price oracle", async function () {
    expect(
      await contractDVPInstance.priceOracles(contractBondInstance.address)
    ).to.eql([]);
    await contractDVPInstance.setPriceOracles(contractBondInstance.address, [
      controller.address,
    ]);
    expect(
      await contractDVPInstance.priceOracles(contractBondInstance.address)
    ).to.eql([controller.address]);
  });

  it("Should set price for pair", async function () {
    try {
      expectRevert(
        await contractDVPInstance.setTokenPrice(
          contractBondInstance.address,
          contractStableInstance.address,
          PARTITION_RESERVED,
          PARTITION_RESERVED,
          "88888"
        )
      );
    } catch (error) {
      //   console.log(error);
    }
    expect(
      await contractDVPInstance.getTokenPrice(
        contractBondInstance.address,
        contractStableInstance.address,
        PARTITION_RESERVED,
        PARTITION_RESERVED
      )
    ).to.equal(0);

    await contractDVPInstance.setPriceOwnership(
      contractBondInstance.address,
      contractStableInstance.address,
      true
    );

    await contractDVPInstance.setTokenPrice(
      contractBondInstance.address,
      contractStableInstance.address,
      PARTITION_RESERVED,
      PARTITION_RESERVED,
      "88888"
    );

    expect(
      await contractDVPInstance.getTokenPrice(
        contractBondInstance.address,
        contractStableInstance.address,
        PARTITION_RESERVED,
        PARTITION_RESERVED
      )
    ).to.equal(88888);
  });
  it("Should set allowance", async function () {
    // enum Standard {OffChain, ETH, ERC20, ERC721, ERC1400, ForgeBond}
    // const deployer = "0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266";
    expect(
      await contractBondInstance.allowance(
        tokenHolder.address,
        contractDVPInstance.address
      )
    ).to.equal(0);

    // await contractBondInstance
    //   .connect(tokenHolder)
    //   .mint(contractDVPInstance.address, 1001);

    const txHolder1 = await contractBondInstance
      .connect(tokenHolder)
      .increaseAllowance(contractDVPInstance.address, 4000000000000000);
    await txHolder1.wait();

    const txHolder2 = await contractStableInstance
      .connect(recipient)
      .increaseAllowance(contractDVPInstance.address, 4000000000000000);
    await txHolder1.wait();

    expect(
      await contractBondInstance.allowance(
        tokenHolder.address,
        contractDVPInstance.address
      )
    ).to.equal(4000000000000000);
  });
  it("Should set a trade", async function () {
    // enum Standard {OffChain, ETH, ERC20, ERC721, ERC1400, ForgeBond}

    // enum State {Undefined, Pending, Executed, Forced, Cancelled}

    // enum TradeType {Escrow, Swap}

    // enum Holder {Holder1, Holder2}

    const trade: TradeRequest = {
      holder1: tokenHolder.address,
      holder2: recipient.address,
      executer: controller.address,
      expirationDate: 1658798074,
      tokenAddress1: contractBondInstance.address,
      tokenValue1: 1000,
      tokenId1: PARTITION_RESERVED,
      tokenStandard1: 2, // "0x45524331343030",
      tokenAddress2: contractStableInstance.address,
      tokenValue2: 3500,
      tokenId2: PARTITION_RESERVED,
      tokenStandard2: 2, // "0x4552433230",
      tradeType: 1,
    };

    const tradeResponse: any = [
      trade.holder1,
      trade.holder2,
      trade.executer,
      {
        _hex: "0x62df3ffa",
        _isBigNumber: true,
      },
      trade.tokenId1,
      trade.tokenId2,
      trade.tradeType,
      1,
    ];

    await contractDVPInstance.setTradeExecuters([controller.address]);

    const newTrade = await (
      await contractDVPInstance.connect(deployer).requestTrade(trade)
    ).wait();
    // console.log(newTrade);

    expect(await contractDVPInstance.getNbTrades()).to.equal(1);

    expect((await contractDVPInstance.getTrade(1))[0]).to.eql(tradeResponse[0]);

    expect(await contractDVPInstance.getPrice(1)).to.equal(3500);
    await (
      await contractDVPInstance.connect(tokenHolder).acceptTrade(1)
    ).wait();
    await (await contractDVPInstance.connect(recipient).acceptTrade(1)).wait();
    // await (await contractDVPInstance.connect(controller).acceptTrade(1)).wait();

    // console.log(await contractDVPInstance.getTradeAcceptanceStatus(1));
    // console.log(await contractDVPInstance.getTradeApprovalStatus(1));
    // console.log(
    await contractDVPInstance.connect(controller).approveTrade(1, true);
    // );

    expect(await contractBondInstance.balanceOf(tokenHolder.address)).to.equal(
      20000
    );
    expect(await contractBondInstance.balanceOf(recipient.address)).to.equal(0);

    expect(
      await contractStableInstance.balanceOf(tokenHolder.address)
    ).to.equal(0);
    expect(await contractStableInstance.balanceOf(recipient.address)).to.equal(
      50000
    );

    await (
      await contractDVPInstance.connect(controller).executeTrade(1)
    ).wait();
    // console.log(await contractDVPInstance.getTradeAcceptanceStatus(1));
    // console.log(await contractDVPInstance.getTradeApprovalStatus(1));

    expect(await contractBondInstance.balanceOf(tokenHolder.address)).to.equal(
      19000
    );
    expect(await contractBondInstance.balanceOf(recipient.address)).to.equal(
      1000
    );

    expect(
      await contractStableInstance.balanceOf(tokenHolder.address)
    ).to.equal(3500);
    expect(await contractStableInstance.balanceOf(recipient.address)).to.equal(
      46500
    );
  });
});
