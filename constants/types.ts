export type BigNumberish = bigint | string | number;
export interface ContractInput {
  initialSupply: BigNumberish;
  isinCode: string;
  name: string;
  symbol: string;
  denomination: BigNumberish;
  divisor: BigNumberish;
  startDate: BigNumberish;
  initialMaturityDate: BigNumberish;
  firstCouponDate: BigNumberish;
  couponFrequencyInMonths: BigNumberish;
  interestRateInBips: BigNumberish;
  callable: boolean;
  isSoftBullet: boolean;
  softBulletPeriodInMonths: BigNumberish;
  currency: string;
  registrar: string;
  settler: string;
  owner: string;
}
