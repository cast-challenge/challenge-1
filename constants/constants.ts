// FORGE
import { ContractInput } from "./types";

export const ERC1820_ACCEPT_MAGIC = "ERC1820_ACCEPT_MAGIC";

export const ERC20_INTERFACE_NAME = "ERC20Token";
export const ERC1400_INTERFACE_NAME = "ERC1400Token";

export const ZERO_BYTES32 =
  "0x0000000000000000000000000000000000000000000000000000000000000000";
export const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";
export const ZERO_BYTE = "0x";

export const partitionFlag =
  "0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"; // Flag to indicate a partition change
export const otherFlag =
  "0xdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"; // Other flag

export const HEX_RESERVED =
  "7265736572766564000000000000000000000000000000000000000000000000"; // reserved in hex
export const HEX_ISSUED =
  "6973737565640000000000000000000000000000000000000000000000000000"; // issued in hex
export const HEX_LOCKED =
  "6c6f636b65640000000000000000000000000000000000000000000000000000"; // locked in hex

export const changeToPartition2 = partitionFlag.concat(HEX_ISSUED);
export const doNotChangePartition = otherFlag.concat(HEX_ISSUED);

export const PARTITION_RESERVED = "0x".concat(HEX_RESERVED);
export const PARTITION_ISSUED = "0x".concat(HEX_ISSUED);
export const PARTITION_LOCKED = "0x".concat(HEX_LOCKED);

export const DEFAULT_PARTITIONS = [
  PARTITION_RESERVED,
  PARTITION_ISSUED,
  PARTITION_LOCKED,
];

export const reversedPartitions = [
  PARTITION_LOCKED,
  PARTITION_ISSUED,
  PARTITION_RESERVED,
];

export const documentName =
  "0x446f63756d656e74204e616d6500000000000000000000000000000000000000";

export const INITIAL_SUPPLY = 1234;

export const DEFAULT_CONTROLLERS = [
  "0xf4fA6bC4aDbB5281275865E9AA6f0b5F492B6DC8",
];
export const FORGE_BOND_CONTRACT_NAME = "ForgeBond";
export const FORGE_BOND_PARAMS: ContractInput = {
  initialSupply: "100000000",
  isinCode: "US0378331005",
  name: "ForgeBond",
  symbol: "FORGEBOND",
  denomination: "1000",
  divisor: "100",
  startDate: "0",
  initialMaturityDate: "0",
  firstCouponDate: "0",
  couponFrequencyInMonths: "0",
  interestRateInBips: "0",
  callable: true,
  isSoftBullet: false,
  softBulletPeriodInMonths: "0",
  currency: "0x0000000000000000000000000000000000000000",
  registrar: "0x0000000000000000000000000000000000000000",
  settler: "0x0000000000000000000000000000000000000000",
  owner: "0x0000000000000000000000000000000000000000",
};
