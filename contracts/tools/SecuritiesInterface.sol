pragma solidity ^0.8.4;
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

import "./DVP.sol";

contract SecuritiesInterface {
    enum Standard {
        OffChain,
        ETH,
        ERC20,
        ERC721,
        ERC1400,
        ForgeBond
    }

    bool internal _ownedContract;
    address internal _contractAddress;
    Standard internal _tokenStandard;

    /**
     * [DVP CONSTRUCTOR]
     * @dev Initialize DVP + register
     * the contract implementation in ERC1820Registry.
     */
    constructor(address contractAddress, Standard tokenStandard) {
        _contractAddress = contractAddress;
        _tokenStandard = tokenStandard;
    }

    /**
     * @dev Accept a given trade (+ potentially escrow tokens).
     * @param index Index of the trade to be accepted.
     */
    function acceptTrade(uint256 index) external payable {
        // _acceptTrade(index, msg.sender, msg.value, 0);
        // if (_tokenStandard == Standard.ETH) {
        //     // require(ethValue == tokenValue, "Amount of ETH is not correct");
        // } else if (_tokenStandard == Standard.ERC20) {
        //     ERC20(tokenAddress).transferFrom(sender, address(this), tokenValue);
        // } else if (_tokenStandard == Standard.ERC721) {
        //     ERC721(tokenAddress).transferFrom(
        //         sender,
        //         address(this),
        //         uint256(tokenId)
        //     );
        // } else if (
        //     _tokenStandard == Standard.ERC1400 && erc1400TokenValue == 0
        // ) {
        //     ERC1400(tokenAddress).operatorTransferByPartition(
        //         tokenId,
        //         sender,
        //         address(this),
        //         tokenValue,
        //         abi.encodePacked(BYPASS_ACTION_FLAG),
        //         abi.encodePacked(BYPASS_ACTION_FLAG)
        //     );
        // } else if (
        //     _tokenStandard == Standard.ERC1400 && erc1400TokenValue != 0
        // ) {
        //     require(
        //         erc1400TokenValue == tokenValue,
        //         "Amount of ERC1400 tokens is not correct"
        //     );
        // } else {
        //     // OffChain
        // }
    }

    /**
     * @dev Accept a given trade (+ potentially escrow tokens).
     * @param index Index of the trade to be accepted.
     */
    function setAllowance(uint256 index) external payable {
          // _acceptTrade(index, msg.sender, msg.value, 0);
        // if (_tokenStandard == Standard.ETH) {
        //     // require(ethValue == tokenValue, "Amount of ETH is not correct");
        // } else if (_tokenStandard == Standard.ERC20) {
        //     ERC20(tokenAddress).transferFrom(sender, address(this), tokenValue);
        // } else if (_tokenStandard == Standard.ERC721) {
        //     ERC721(tokenAddress).transferFrom(
        //         sender,
        //         address(this),
        //         uint256(tokenId)
        //     );
        // } else if (
        //     _tokenStandard == Standard.ERC1400 && erc1400TokenValue == 0
        // ) {
        //     ERC1400(tokenAddress).operatorTransferByPartition(
        //         tokenId,
        //         sender,
        //         address(this),
        //         tokenValue,
        //         abi.encodePacked(BYPASS_ACTION_FLAG),
        //         abi.encodePacked(BYPASS_ACTION_FLAG)
        //     );
        // } else if (
        //     _tokenStandard == Standard.ERC1400 && erc1400TokenValue != 0
        // ) {
        //     require(
        //         erc1400TokenValue == tokenValue,
        //         "Amount of ERC1400 tokens is not correct"
        //     );
        // } else {
        //     // OffChain
        // }
    }

    /**
     * @dev Accept a given trade (+ potentially escrow tokens).
     * @param index Index of the trade to be accepted.
     */
    function issue(uint256 index) external payable {
          // _acceptTrade(index, msg.sender, msg.value, 0);
        // if (_tokenStandard == Standard.ETH) {
        //     // require(ethValue == tokenValue, "Amount of ETH is not correct");
        // } else if (_tokenStandard == Standard.ERC20) {
        //     ERC20(tokenAddress).transferFrom(sender, address(this), tokenValue);
        // } else if (_tokenStandard == Standard.ERC721) {
        //     ERC721(tokenAddress).transferFrom(
        //         sender,
        //         address(this),
        //         uint256(tokenId)
        //     );
        // } else if (
        //     _tokenStandard == Standard.ERC1400 && erc1400TokenValue == 0
        // ) {
        //     ERC1400(tokenAddress).operatorTransferByPartition(
        //         tokenId,
        //         sender,
        //         address(this),
        //         tokenValue,
        //         abi.encodePacked(BYPASS_ACTION_FLAG),
        //         abi.encodePacked(BYPASS_ACTION_FLAG)
        //     );
        // } else if (
        //     _tokenStandard == Standard.ERC1400 && erc1400TokenValue != 0
        // ) {
        //     require(
        //         erc1400TokenValue == tokenValue,
        //         "Amount of ERC1400 tokens is not correct"
        //     );
        // } else {
        //     // OffChain
        // }
    }

    /**
     * @dev Accept a given trade (+ potentially escrow tokens).
     * @param index Index of the trade to be accepted.
     */
    function transfer(uint256 index) external payable {
          // _acceptTrade(index, msg.sender, msg.value, 0);
        // if (_tokenStandard == Standard.ETH) {
        //     // require(ethValue == tokenValue, "Amount of ETH is not correct");
        // } else if (_tokenStandard == Standard.ERC20) {
        //     ERC20(tokenAddress).transferFrom(sender, address(this), tokenValue);
        // } else if (_tokenStandard == Standard.ERC721) {
        //     ERC721(tokenAddress).transferFrom(
        //         sender,
        //         address(this),
        //         uint256(tokenId)
        //     );
        // } else if (
        //     _tokenStandard == Standard.ERC1400 && erc1400TokenValue == 0
        // ) {
        //     ERC1400(tokenAddress).operatorTransferByPartition(
        //         tokenId,
        //         sender,
        //         address(this),
        //         tokenValue,
        //         abi.encodePacked(BYPASS_ACTION_FLAG),
        //         abi.encodePacked(BYPASS_ACTION_FLAG)
        //     );
        // } else if (
        //     _tokenStandard == Standard.ERC1400 && erc1400TokenValue != 0
        // ) {
        //     require(
        //         erc1400TokenValue == tokenValue,
        //         "Amount of ERC1400 tokens is not correct"
        //     );
        // } else {
        //     // OffChain
        // }
    }

    /**
     * @dev Accept a given trade (+ potentially escrow tokens).
     * @param index Index of the trade to be accepted.
     */
    function burn(uint256 index) external payable {
          // _acceptTrade(index, msg.sender, msg.value, 0);
        // if (_tokenStandard == Standard.ETH) {
        //     // require(ethValue == tokenValue, "Amount of ETH is not correct");
        // } else if (_tokenStandard == Standard.ERC20) {
        //     ERC20(tokenAddress).transferFrom(sender, address(this), tokenValue);
        // } else if (_tokenStandard == Standard.ERC721) {
        //     ERC721(tokenAddress).transferFrom(
        //         sender,
        //         address(this),
        //         uint256(tokenId)
        //     );
        // } else if (
        //     _tokenStandard == Standard.ERC1400 && erc1400TokenValue == 0
        // ) {
        //     ERC1400(tokenAddress).operatorTransferByPartition(
        //         tokenId,
        //         sender,
        //         address(this),
        //         tokenValue,
        //         abi.encodePacked(BYPASS_ACTION_FLAG),
        //         abi.encodePacked(BYPASS_ACTION_FLAG)
        //     );
        // } else if (
        //     _tokenStandard == Standard.ERC1400 && erc1400TokenValue != 0
        // ) {
        //     require(
        //         erc1400TokenValue == tokenValue,
        //         "Amount of ERC1400 tokens is not correct"
        //     );
        // } else {
        //     // OffChain
        // }
    }
}
