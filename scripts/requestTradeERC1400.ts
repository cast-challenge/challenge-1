// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
// import { PARTITION_RESERVED } from "../constants/constants";
const hre = require("hardhat");
const ethers = hre.ethers;

const DEPLOYMENT_PATH = "./../frontend/deployments/localhost/";
const DEPLOYMENTS = {
  DVP: require(DEPLOYMENT_PATH + "DVP.json"),
  ForgeBond: require(DEPLOYMENT_PATH + "ForgeBond.json"),
  ERC1400: require(DEPLOYMENT_PATH + "ERC1400.json"),
  ERC20: require(DEPLOYMENT_PATH + "ERC20Token.json"),
  OperatorManagerLibrary: require(DEPLOYMENT_PATH +
    "OperatorManagerLibrary.json"),
  SettlementWorkflowLibrary: require(DEPLOYMENT_PATH +
    "SettlementWorkflowLibrary.json"),
  SettlementRepositoryLibrary: require(DEPLOYMENT_PATH +
    "SettlementRepositoryLibrary.json"),
  SecurityTokenBalancesLibrary: require(DEPLOYMENT_PATH +
    "SecurityTokenBalancesLibrary.json"),
};
const CONSTANTS = require("./../constants/constants.ts");

async function main(hre: HardhatRuntimeEnvironment) {
  const { getNamedAccounts } = hre;
  const { deploy } = hre.deployments;

  const {
    OWNER_ACCOUNT: OWNER_ACCOUNT_ADDRESS,
    HOLDER_ACCOUNT_1: HOLDER_ACCOUNT_1_ADDRESS,
    // HOLDER_ACCOUNT_1,
    CONTROLLER_ACCOUNT,
  } = await getNamedAccounts();
  const accounts = await hre.ethers.getSigners();
  // const signers = await hre.ethers.getSigners();
  const OWNER_ACCOUNT =
    accounts.find((account) => account.address === OWNER_ACCOUNT_ADDRESS) ??
    accounts[10];

  const HOLDER_ACCOUNT_1 =
    accounts.find((account) => account.address === HOLDER_ACCOUNT_1_ADDRESS) ??
    accounts[11];
  //   console.log(accounts);
  //   console.log(OWNER_ACCOUNT_ADDRESS);
  //   console.log(OWNER_ACCOUNT);
  const DVP = await hre.deployments.get("DVP");
  const ERC1400 = await hre.deployments.get("ERC1400");
  const ERC20Token = await hre.deployments.get("ERC20Token");
  //   const DVP = await ethers.getContractFactory("DVP", {
  //     from: OWNER_ACCOUNT_ADDRESS,
  //   });

  console.log(DVP.address);
  console.log(ERC20Token.address);

  const contractDVPInstance = await hre.ethers.getContractAt(
    "DVP",
    DVP.address
  );

  const trade = {
    holder1: OWNER_ACCOUNT_ADDRESS,
    holder2: HOLDER_ACCOUNT_1.address,
    executer: CONTROLLER_ACCOUNT,
    expirationDate: 1658798074,
    tokenAddress1: ERC1400.address,
    tokenValue1: hre.ethers.utils.parseUnits("1", 18),
    tokenId1: CONSTANTS.PARTITION_RESERVED,
    tokenStandard1: 4, // "0x45524331343030",
    tokenAddress2: ERC20Token.address,
    tokenValue2: hre.ethers.utils.parseUnits("1", 0),
    tokenId2: CONSTANTS.PARTITION_RESERVED,
    tokenStandard2: 2, // "0x4552433230",
    tradeType: 1,
  };
  // const tradeHash = await contractDVPInstance.getTrade(0);
  // console.log(tradeHash);
  console.log(trade);

  await contractDVPInstance.connect(OWNER_ACCOUNT).requestTrade(trade, {
    from: OWNER_ACCOUNT_ADDRESS,
  });

  const nb = await contractDVPInstance.getNbTrades();
  console.log("nb: ", nb.toNumber());
}
// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main(hre).catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
