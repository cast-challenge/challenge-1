import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
const hre = require("hardhat");

// eslint-disable-next-line max-len

async function main(hre: HardhatRuntimeEnvironment) {
  const { web3, getNamedAccounts } = hre;
  const {
    OWNER_ACCOUNT,

    CONTROLLER_ACCOUNT,
    HOLDER_ACCOUNT_1,
    HOLDER_ACCOUNT_2,

    ERC1820_DEPLOYER_ADDRESS,
  } = await getNamedAccounts();
  //   const OWNER_ACCOUNT = accounts[10];

  console.log("\n   >Send ethers: Start -->" + OWNER_ACCOUNT);

  // @see https://eips.ethereum.org/EIPS/eip-1820
  // We need to deploy ERC1820Register constract with a specific address
  // Before deploy contract we transfer some ETH to pay the gas
  await web3.eth.sendTransaction({
    from: OWNER_ACCOUNT,
    to: ERC1820_DEPLOYER_ADDRESS,
    value: web3.utils.toWei("1"),
  });
  await web3.eth.sendTransaction({
    from: OWNER_ACCOUNT,
    to: HOLDER_ACCOUNT_1,
    value: web3.utils.toWei("1"),
  });
  await web3.eth.sendTransaction({
    from: OWNER_ACCOUNT,
    to: HOLDER_ACCOUNT_2,
    value: web3.utils.toWei("1"),
  });
  await web3.eth.sendTransaction({
    from: OWNER_ACCOUNT,
    to: CONTROLLER_ACCOUNT,
    value: web3.utils.toWei("1"),
  });

  console.log("\n   >Send ethers: Success -->");
}
main(hre).catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
