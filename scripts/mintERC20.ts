// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { Wallet } from "ethers";
import { PARTITION_RESERVED } from "../constants/constants";
// import { PARTITION_RESERVED } from "../constants/constants";
const hre = require("hardhat");
const ethers = hre.ethers;

const DEPLOYMENT_PATH = "./../frontend/deployments/localhost/";
const DEPLOYMENTS = {
  DVP: require(DEPLOYMENT_PATH + "DVP.json"),
  ForgeBond: require(DEPLOYMENT_PATH + "ForgeBond.json"),
  ERC1400: require(DEPLOYMENT_PATH + "ERC1400.json"),
  ERC20: require(DEPLOYMENT_PATH + "ERC20Token.json"),
  OperatorManagerLibrary: require(DEPLOYMENT_PATH +
    "OperatorManagerLibrary.json"),
  SettlementWorkflowLibrary: require(DEPLOYMENT_PATH +
    "SettlementWorkflowLibrary.json"),
  SettlementRepositoryLibrary: require(DEPLOYMENT_PATH +
    "SettlementRepositoryLibrary.json"),
  SecurityTokenBalancesLibrary: require(DEPLOYMENT_PATH +
    "SecurityTokenBalancesLibrary.json"),
};
const CONSTANTS = require("./../constants/constants.ts");

async function main(hre: HardhatRuntimeEnvironment) {
  const { getNamedAccounts } = hre;
  const { deploy } = hre.deployments;
  //   0x22036D131B6F13Df73417a98D34b31C68B04194a
  // const { HOLDER_ACCOUNT_2 } = await getNamedAccounts();
  const {
    OWNER_ACCOUNT: OWNER_ACCOUNT_ADDRESS,
    HOLDER_ACCOUNT_1: HOLDER_ACCOUNT_1_ADDRESS,
    HOLDER_ACCOUNT_2: HOLDER_ACCOUNT_2_ADDRESS,
    // HOLDER_ACCOUNT_1,
    CONTROLLER_ACCOUNT,
  } = await getNamedAccounts();
  const accounts = await hre.ethers.getSigners();
  // const signers = await hre.ethers.getSigners();
  const OWNER_ACCOUNT =
    accounts.find((account) => account.address === OWNER_ACCOUNT_ADDRESS) ??
    accounts[10];

  const HOLDER_ACCOUNT_1 =
    accounts.find((account) => account.address === HOLDER_ACCOUNT_1_ADDRESS) ??
    accounts[11];

  const HOLDER_ACCOUNT_2 =
    accounts.find((account) => account.address === HOLDER_ACCOUNT_2_ADDRESS) ??
    accounts[11];
  const DVP = await hre.deployments.get("DVP");
  const ForgeBond = await hre.deployments.get("ForgeBond");
  const ERC20Token = await hre.deployments.get("ERC20Token");
  const ERC1400 = await hre.deployments.get("ERC1400");

  const contractDVPInstance = await hre.ethers.getContractAt(
    "DVP",
    DVP.address
  );
  const contractERC20Instance = await hre.ethers.getContractAt(
    "ERC20Token",
    ERC20Token.address
  );
  const contractERC1400Instance = await hre.ethers.getContractAt(
    "ERC1400",
    ERC1400.address
  );
  const contractBondInstance = await hre.ethers.getContractAt(
    "ForgeBond",
    ForgeBond.address
  );

  await (
    await contractERC20Instance
      .connect(OWNER_ACCOUNT)
      .mint(HOLDER_ACCOUNT_1.address, hre.ethers.utils.parseUnits("50000", 0), {
        from: await OWNER_ACCOUNT?.getAddress(),
      })
  ).wait();
  await (
    await contractERC20Instance
      .connect(OWNER_ACCOUNT)
      .mint(HOLDER_ACCOUNT_2.address, hre.ethers.utils.parseUnits("60000", 0), {
        from: await OWNER_ACCOUNT?.getAddress(),
      })
  ).wait();
  await (
    await contractERC20Instance
      .connect(OWNER_ACCOUNT)
      .mint(OWNER_ACCOUNT.address, hre.ethers.utils.parseUnits("60000", 0), {
        from: await OWNER_ACCOUNT?.getAddress(),
      })
  ).wait();
  await (
    await contractERC1400Instance
      .connect(OWNER_ACCOUNT)
      .issue(
        OWNER_ACCOUNT.address,
        hre.ethers.utils.parseUnits("1000000", 18),
        PARTITION_RESERVED,
        {
          from: await OWNER_ACCOUNT?.getAddress(),
        }
      )
  ).wait();
  //   await (
  //     await contractBondInstance
  //       .connect(OWNER_ACCOUNT)
  //       .authorizeOperator("2", DVP.address, {
  //         from: await OWNER_ACCOUNT?.getAddress(),
  //       })
  //   ).wait();
  //   await (
  //     await contractERC20Instance
  //       .connect(HOLDER_ACCOUNT_1)
  //       .approve(DVP.address, "100000000000000000000", {
  //         from: await HOLDER_ACCOUNT_1.getAddress(),
  //       })
  //   ).wait();

  console.log(
    contractERC20Instance.address,
    await contractERC20Instance.balanceOf(HOLDER_ACCOUNT_1.address),
    await contractBondInstance.getFullBalances()
  );
}
// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main(hre).catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
