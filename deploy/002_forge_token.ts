import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { FORGE_BOND_PARAMS } from "../constants/constants";
import { artifacts } from "hardhat";
import { ContractInput } from "../constants/types";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;
  const { owner, OWNER_ACCOUNT } = await getNamedAccounts();

  const libOperatorManagerLibrary = await deploy("OperatorManagerLibrary", {
    from: OWNER_ACCOUNT,
    args: [],
    log: true,
  });

  const libIterableBalances = await deploy("IterableBalances", {
    from: OWNER_ACCOUNT,
    args: [],
    log: true,
  });

  const libSecurityTokenBalancesLibrary = await deploy(
    "SecurityTokenBalancesLibrary",
    {
      from: OWNER_ACCOUNT,
      args: [],
      log: true,
      libraries: {
        IterableBalances: libIterableBalances.address,
      },
    }
  );
  const libSettlementRepositoryLibrary = await deploy(
    "SettlementRepositoryLibrary",
    {
      from: OWNER_ACCOUNT,
      args: [],
      log: true,
    }
  );

  const libSettlementWorkflowLibrary = await deploy(
    "SettlementWorkflowLibrary",
    {
      from: OWNER_ACCOUNT,
      args: [],
      log: true,
      libraries: {
        SecurityTokenBalancesLibrary: libSecurityTokenBalancesLibrary.address,
        SettlementRepositoryLibrary: libSettlementRepositoryLibrary.address,
      },
    }
  );
  const ERC1820Registry = await artifacts.readArtifact("IERC1820Registry");
  console.log(ERC1820Registry.linkReferences);

  const deployParams: Partial<ContractInput> = {
    registrar: OWNER_ACCOUNT,
    settler: OWNER_ACCOUNT,
    owner: OWNER_ACCOUNT,
  };
  // eslint-disable-next-line node/no-unsupported-features/es-syntax
  const forgeBond: ContractInput = {
    ...FORGE_BOND_PARAMS,
    ...deployParams,
  };
  console.log(forgeBond);

  const ForgeBondContract = await deploy("ForgeBond", {
    from: OWNER_ACCOUNT,
    args: [forgeBond],
    log: true,
    libraries: {
      OperatorManagerLibrary: libOperatorManagerLibrary.address,
      SettlementWorkflowLibrary: libSettlementWorkflowLibrary.address,
      SettlementRepositoryLibrary: libSettlementRepositoryLibrary.address,
      SecurityTokenBalancesLibrary: libSecurityTokenBalancesLibrary.address,
    },
  });
  console.log(
    "\n   > ForgeBond deployment: Success -->",
    ForgeBondContract?.address
  );
};
export default func;
func.tags = ["ForgeBond"];
