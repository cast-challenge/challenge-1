import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;
  const { owner, OWNER_ACCOUNT } = await getNamedAccounts();

  const DVPContract = await deploy("DVP", {
    from: OWNER_ACCOUNT,
    args: [false, false],
    log: true,
  });

  console.log(
    "\n   > DVPContract deployment: Success -->",
    DVPContract?.address
  );
};
export default func;
func.tags = ["DVPContract"];
