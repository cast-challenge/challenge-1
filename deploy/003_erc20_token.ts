import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;
  const { owner, OWNER_ACCOUNT } = await getNamedAccounts();

  const ERC20Contract = await deploy("ERC20Token", {
    from: OWNER_ACCOUNT,
    args: ["ERC20Token", "ERC20USD", 1],
    log: true,
  });
  console.log(
    "\n   > ERC20Token deployment: Success -->",
    ERC20Contract?.address
  );
};
export default func;
func.tags = ["ERC20Token"];
