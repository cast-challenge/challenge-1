import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;
  const { owner, OWNER_ACCOUNT } = await getNamedAccounts();

  //   const partition1 =
  //     "0x7265736572766564000000000000000000000000000000000000000000000000"; // reserved in hex
  //   const partition2 =
  //     "0x6973737565640000000000000000000000000000000000000000000000000000"; // issued in hex
  //   const partition3 =
  //     "0x6c6f636b65640000000000000000000000000000000000000000000000000000"; // locked in hex
  //   const partitions = [partition1, partition2, partition3];

  //   await deploy("SecuritiesInterface", {
  //     from: owner,
  //     args: ["0x00000", "CO2", 1, [controller], partitions],
  //     log: true,
  //   });
};
export default func;
func.tags = ["SecuritiesInterface"];
